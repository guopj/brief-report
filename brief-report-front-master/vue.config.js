const { defineConfig } = require('@vue/cli-service')
//在这里定义一些vue的一些配置（类似我们后端的yaml文件）
module.exports = defineConfig({
  transpileDependencies: true,
  devServer :{
    port: 39052
  },
  chainWebpack:config =>{
    config.plugin('html')
    .tap(
      args=>{
        args[0].title ="BriefReport-简捷报";
        return args;
      }
    )
  }
}
)
