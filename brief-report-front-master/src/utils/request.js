//封装好的前端向后端发起的request请求，方便管理
import axios from 'axios'
import router from "@/router";

const request = axios.create({
    baseURL: 'http://localhost:39051', //设置后端的默认请求地址
    timeout: 300000000
})

// request 拦截器
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
    //kinroy 2023.12.10 每次发送请求都会携带token，完成鉴权,从localStrorage中获取
    let loginUser = JSON.parse(localStorage.getItem("loginUser")||'{}');
    //把token设置到请求头中，每次请求都会携带token
     config.headers['token'] = loginUser.token;
    return config
}, error => {
    return Promise.reject(error)
});

// response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        if (response.config.responseType === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        //设置登录权限，不满足则跳转
        if (res.code==='401'){
            router.push("/login")
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)


export default request
