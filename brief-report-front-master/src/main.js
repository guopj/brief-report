// 这里是vue框架下导入全局配置的地方
import Vue from 'vue'
import App from './App.vue'
import router from './router'
//引入elementUi
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 引入我们自己定义的全局css
import '@/assets/css/global.css';
import request from "@/utils/request"
//引入我们的外部icon图标
import './assets/icon/iconfont.css';
import './assets/icon/iconfont.js';
//引入我们的md富文本组件
import VueMarkdownEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';
//引入代码高亮
// highlightjs 核心代码
import hljs from 'highlight.js/lib/core';
// 按需引入语言包
import java from 'highlight.js/lib/languages/java';
import json from 'highlight.js/lib/languages/java';

hljs.registerLanguage('json', json);
hljs.registerLanguage('java', java);
import Prism from 'prismjs';

VueMarkdownEditor.use(githubTheme, {
    Hljs: hljs,
});

VueMarkdownEditor.use(vuepressTheme, {
    Prism,
});

Vue.use(VueMarkdownEditor);

Vue.config.productionTip = false
Vue.use(ElementUI, {size: 'small'}); //使用elementUI

Vue.prototype.$request = request
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
