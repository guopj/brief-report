//在这里写好对应的路由配置（访问哪一个url，对应跳到哪一个vue中）
import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'manager',
        component: () => import('../views/Manager.vue'),
        //  配置子路由，能够在切换不同的main
        children: [
            {
                path: 'userInformation', name: 'userInformation',
                component: () => import('../views/UserInformation.vue'),
                meta:{
                    title:'个人信息',
                    path:"/userInformation"
                }
            },
            {
                path: '403', name: 'auth',
                component: () => import('../views/Auth.vue'),
                meta:{
                    title:'无权访问',
                    path:"/403"
                }
            },
            {
                path: 'workbench', name: 'workbench',
                component: () => import('../views/Workbench.vue'),
                meta:{
                    title:'工作台',
                    path:"/workbench"
                }
            },
            {
                path: 'notice', name: 'notice',
                component: () => import('../views/Notice.vue'),
                meta:{
                    title:'消息',
                    path:"/notice"
                }
            },
            //任务中心路由
            {
                path: 'mytasks', name: 'myTasks',
                component: () => import('../views/taskCenter/MyTasks.vue'),
                meta:{
                    title:'我的任务',
                    path:"/myTasks"
                }
            },
            {
                path: 'taskSummary', name: 'taskSummary',
                component: () => import('../views/taskCenter/TaskSummary.vue'),
                meta:{
                    title:'任务汇总',
                    path:"/taskSummary"
                }
            },
            {
                path: 'taskManagement',
                name: 'taskManagement',
                component: () => import('../views/taskCenter/TaskManagement.vue'),
                meta:{
                    title:'任务管理',
                    path:"/taskManagement"
                }
            },
            //日志中心路由
            {
                path: 'noteManagement',
                name: 'noteManagement',
                component: () => import('../views/noteCenter/NoteManagement.vue'),
                meta:{
                    title:'日志管理',
                    path:"/noteManagement"
                }
            },
            {
                path: 'myNotes',
                name: 'myNotes.vue',
                component: () => import('../views/noteCenter/MyNotes.vue'),
                meta:{
                    title:'我的日志',
                    path:"/myNotes"
                }
            },
            //管理中心路由
            {
                path: 'allNoteManagement',
                name: 'allNoteManagement',
                component: () => import('../views/managementCenter/AllNoteManagement.vue'),
                meta:{
                    title:'总体日志管理',
                    path:"/allNoteManagement"
                }
            },
            {
                path: 'employeeManagement',
                name: 'employeeManagement',
                component: () => import('../views/managementCenter/EmployeeManagement.vue'),
                meta:{
                    title:'员工管理',
                    path:"/employeeManagement"
                }
            },
            {
                path: 'projectManagement',
                name: 'projectManagement',
                component: () => import('../views/managementCenter/ProjectManagement.vue'),
                meta:{
                    title:'项目管理',
                    path:"/projectManagement"
                }
            }

        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('../views/Register.vue')
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

//配置路由守卫，做页面鉴权
router.beforeEach((to, from, next) => {
    //需要鉴权的页面
    let authenticationPaths = ['/taskManagement', '/allNoteManagement', '/allTaskManagement',
        '/employeeManagement', '/handOverNodesManagement', '/projectManagement'
    ]
    //获取当前用户权限
    let userRole = localStorage.getItem("loginUserRole")
    //判断 当前用户是普通员工，但是访问了要鉴权的页面
    if (userRole === '3' && authenticationPaths.includes(to.path)) {
        next('/403')
    } else {
        next()
    }
})

export default router
