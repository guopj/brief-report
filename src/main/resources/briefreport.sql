/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-mysql
 Source Server Type    : MySQL
 Source Server Version : 50736 (5.7.36)
 Source Host           : 112.124.13.174:3306
 Source Schema         : briefreport

 Target Server Type    : MySQL
 Target Server Version : 50736 (5.7.36)
 File Encoding         : 65001

 Date: 01/06/2024 00:14:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `department_user_relation`;
CREATE TABLE `department_user_relation` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `department_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for note_image_relation
-- ----------------------------
DROP TABLE IF EXISTS `note_image_relation`;
CREATE TABLE `note_image_relation` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `note_id` varchar(128) DEFAULT NULL COMMENT '日志Id',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for note_log
-- ----------------------------
DROP TABLE IF EXISTS `note_log`;
CREATE TABLE `note_log` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `note_id` varchar(128) DEFAULT NULL COMMENT '日志Id',
  `operation` varchar(255) DEFAULT NULL COMMENT '操作',
  `operation_version` int(11) DEFAULT NULL COMMENT '操作版本号',
  `operation_time` timestamp NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for project_task_relation
-- ----------------------------
DROP TABLE IF EXISTS `project_task_relation`;
CREATE TABLE `project_task_relation` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `project_id` varchar(128) DEFAULT NULL COMMENT '项目实体id（UUID）',
  `task_id` varchar(128) DEFAULT NULL COMMENT '任务实体id（UUID）',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for project_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `project_user_relation`;
CREATE TABLE `project_user_relation` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `project_id` varchar(128) DEFAULT NULL COMMENT '项目实体id（UUID）',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_department
-- ----------------------------
DROP TABLE IF EXISTS `tb_department`;
CREATE TABLE `tb_department` (
  `department_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `department_name` varchar(64) DEFAULT NULL COMMENT '部门名称',
  `department_info` text COMMENT '部门信息',
  `department_manager` varchar(255) DEFAULT NULL COMMENT '部门管理人集合',
  PRIMARY KEY (`department_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_handovernode
-- ----------------------------
DROP TABLE IF EXISTS `tb_handovernode`;
CREATE TABLE `tb_handovernode` (
  `node_id` varchar(128) NOT NULL COMMENT '任务流转节点实体id（UUID）',
  `task_handlerid` int(11) NOT NULL DEFAULT '0' COMMENT '任务创建者主键',
  `task_status` int(11) DEFAULT NULL COMMENT '当前工作状态',
  `task_id` varchar(128) DEFAULT NULL COMMENT '任务id',
  `handover_num` int(11) DEFAULT NULL COMMENT '流转序号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '任务创建时间',
  `task_handlerName` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`node_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu` (
  `mid` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单节点id',
  `mname` varchar(32) NOT NULL COMMENT '菜单节点名称',
  `mdesc` varchar(128) DEFAULT NULL COMMENT '菜单节点描述',
  `parentid` int(11) NOT NULL COMMENT '父节点id',
  `murl` varchar(200) DEFAULT NULL COMMENT '菜单节点点击后跳转的url',
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_note
-- ----------------------------
DROP TABLE IF EXISTS `tb_note`;
CREATE TABLE `tb_note` (
  `note_id` varchar(128) NOT NULL COMMENT '日志id（UUID）',
  `note_name` varchar(255) NOT NULL COMMENT '日志名称',
  `note_type` int(11) NOT NULL COMMENT '日志类型',
  `publish_userid` int(11) NOT NULL COMMENT '发布者Id',
  `note_content` text COMMENT '日志文本内容',
  `note_status` int(11) NOT NULL COMMENT '日志状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `publish_time` timestamp NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`note_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_project
-- ----------------------------
DROP TABLE IF EXISTS `tb_project`;
CREATE TABLE `tb_project` (
  `project_id` varchar(128) NOT NULL COMMENT '项目实体id（UUID）',
  `project_name` varchar(128) NOT NULL DEFAULT '' COMMENT '项目名称',
  `create_userId` int(11) NOT NULL DEFAULT '0' COMMENT '项目创建者主键',
  `project_leaderId` int(11) NOT NULL DEFAULT '0' COMMENT '项目组长主键',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '项目创建时间',
  `completion_time` timestamp NULL DEFAULT NULL COMMENT '项目完成时间',
  `project_status` int(11) NOT NULL DEFAULT '0' COMMENT '当前项目状态',
  PRIMARY KEY (`project_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_remindmessage
-- ----------------------------
DROP TABLE IF EXISTS `tb_remindmessage`;
CREATE TABLE `tb_remindmessage` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) NOT NULL COMMENT '消息归属人Id',
  `msg_content` text COMMENT '消息内容',
  `msg_status` int(11) NOT NULL COMMENT '消息状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1488 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rname` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `rdesc` varchar(128) DEFAULT '' COMMENT '角色描述',
  PRIMARY KEY (`rid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色信息表';

-- ----------------------------
-- Table structure for tb_task
-- ----------------------------
DROP TABLE IF EXISTS `tb_task`;
CREATE TABLE `tb_task` (
  `task_id` varchar(128) NOT NULL COMMENT '任务实体id（UUID）',
  `task_type` int(11) NOT NULL DEFAULT '0' COMMENT '任务类型',
  `task_Name` varchar(128) NOT NULL DEFAULT '' COMMENT '任务名称',
  `create_userId` int(11) NOT NULL DEFAULT '0' COMMENT '任务创建者主键',
  `current_taskHandlerId` int(11) NOT NULL DEFAULT '0' COMMENT '当前处理用户主键',
  `taskHandover_path` text COMMENT '任务流转',
  `task_status` int(11) NOT NULL DEFAULT '0' COMMENT '当前工作状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '任务创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '任务创建时间',
  `deadline_time` timestamp NULL DEFAULT NULL COMMENT '任务截至时间',
  `completion_time` timestamp NULL DEFAULT NULL COMMENT '任务完成时间',
  `project_id` varchar(128) NOT NULL DEFAULT '' COMMENT '归属项目id',
  `task_priority` int(11) DEFAULT NULL COMMENT '项目优先级',
  `restart_times` int(11) NOT NULL DEFAULT '0' COMMENT '任务重开次数',
  `task_description` text COMMENT '任务描述',
  `task_solution` text COMMENT '任务解决方案',
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tb_todoevent
-- ----------------------------
DROP TABLE IF EXISTS `tb_todoevent`;
CREATE TABLE `tb_todoevent` (
  `event_id` varchar(128) NOT NULL COMMENT '待办事务id（UUID）',
  `event_context` text NOT NULL COMMENT '待办事务文本描述',
  `event_status` int(11) NOT NULL DEFAULT '0' COMMENT '待办事务状态 0:未完成 1:完成',
  `user_id` int(11) NOT NULL COMMENT '发布者Id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deadline_time` timestamp NULL DEFAULT NULL COMMENT '需完成时间',
  PRIMARY KEY (`event_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `salt` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码、通信等加密盐',
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码,md5加密',
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户信息表';

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `gender` varchar(16) DEFAULT NULL COMMENT '性别',
  `avatar_image` text COMMENT '头像url',
  `introduction` text COMMENT '个人介绍',
  `info_name` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `department_id` varchar(64) DEFAULT NULL,
  `job_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for user_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `user_role_relation`;
CREATE TABLE `user_role_relation` (
  `rid` int(11) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
