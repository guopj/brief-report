package com.kinroy.briefreport.config;

import com.kinroy.briefreport.interceptor.JwtInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
 registry.addInterceptor(jwtInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login/userlogin")
         .excludePathPatterns("/login/register")
                .excludePathPatterns("/login/sendcode").excludePathPatterns("/login/checkUserNameOrPhone")
                .excludePathPatterns("/login/fasterrigester").excludePathPatterns("/login/getUserRoles")
        ;
        super.addInterceptors(registry);

    }

    @Bean
    public JwtInterceptor jwtInterceptor() {
        return new JwtInterceptor();
    }

}
