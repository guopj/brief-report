package com.kinroy.briefreport.config;

import com.kinroy.briefreport.properties.MinIOConfigProperties;
import com.kinroy.briefreport.service.FileStorageService;
import io.minio.MinioClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 文件上传 minio相关配置类
 *
 * @author kinroy
 */
@Data
@Configuration
@EnableConfigurationProperties({MinIOConfigProperties.class})
@ConditionalOnClass(FileStorageService.class)
public class MinIOConfig {
    //直接注入我们封装好的minio相关的yml的属性配置类
    @Autowired
    private MinIOConfigProperties minIOConfigProperties;

    //在spring容器启动时，便创建好minio的客户端，属性从属性配置类中取出
    @Bean
    public MinioClient buildMinioClient() {
        //credentials中填账号密码   endpoint填minio的url
        return MinioClient
                .builder()
                .credentials(minIOConfigProperties.getAccessKey(), minIOConfigProperties.getSecretKey())
                .endpoint(minIOConfigProperties.getEndpoint())
                .build();
    }
}
