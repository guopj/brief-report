package com.kinroy.briefreport.config;

import cn.hutool.json.JSONConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * json字符串转换成bean时，一些数据类型的格式配置
 *
 * @author kinroy
 */
@Configuration
public class JsonConfigs {

    @Bean("dateTimeConfig")
    public static JSONConfig dateTimeConfig() {
        //前端返回的时间数组格式为[yyyy,mm,dd,hh,min,sc],所有这里也要设置相应的格式
        /*Parse [[2023,12,12,21,5,4]] with format [yyyy-MM-dd HH:mm:ss] error!*/
        JSONConfig jsonConfig = JSONConfig.create().setDateFormat("[yyyy,MM,dd,HH,mm,ss]");
        return jsonConfig;
    }
}
