package com.kinroy.briefreport.dto;

import com.kinroy.briefreport.entity.Project;

/**
 * 项目信息实体
 * @author kinroy
 */
public class ProjectInfoDto extends Project {
    //新增属性
    private String createUserName;
    private String projectLeaderName;
    private String projectStatusName;


    public ProjectInfoDto() {
    }


    /**
     * 获取
     * @return createUserName
     */
    public String getCreateUserName() {
        return createUserName;
    }

    /**
     * 设置
     * @param createUserName
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    /**
     * 获取
     * @return projectLeaderName
     */
    public String getProjectLeaderName() {
        return projectLeaderName;
    }

    /**
     * 设置
     * @param projectLeaderName
     */
    public void setProjectLeaderName(String projectLeaderName) {
        this.projectLeaderName = projectLeaderName;
    }

    /**
     * 获取
     * @return projectStatusName
     */
    public String getProjectStatusName() {
        return projectStatusName;
    }

    /**
     * 设置
     * @param projectStatusName
     */
    public void setProjectStatusName(String projectStatusName) {
        this.projectStatusName = projectStatusName;
    }

    @Override
    public String toString() {
        return "ProjectInfoDto{createUserName = " + createUserName + ", projectLeaderName = " + projectLeaderName + ", projectStatusName = " + projectStatusName + "}";
    }
}
