package com.kinroy.briefreport.dto;

/**
 * @author kinroy
 */
public class UserDto {
    private Long uid;
    private String name;
    private String phone;
    private String password;
    private int loginType; //0:账号登录   1：手机登录
    private String code;
    private String token; //鉴权校验用的token


    public UserDto() {
    }

    public UserDto(Long uid, String name, String phone, String password, int loginType, String code, String token) {
        this.uid = uid;
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.loginType = loginType;
        this.code = code;
        this.token = token;
    }

    /**
     * 获取
     * @return uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置
     * @param uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取
     * @return loginType
     */
    public int getLoginType() {
        return loginType;
    }

    /**
     * 设置
     * @param loginType
     */
    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    /**
     * 获取
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * 设置
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserDto{uid = " + uid + ", name = " + name + ", phone = " + phone + ", password = " + password + ", loginType = " + loginType + ", code = " + code + ", token = " + token + "}";
    }
}
