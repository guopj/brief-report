package com.kinroy.briefreport.dto;

import com.kinroy.briefreport.entity.Note;

import java.time.LocalDateTime;

/**
 * 日志实体的Dto
 * @author kinroy
 */
public class NoteDto extends Note {
    //前端传来的发布时间分成两个部分： 1.日期  2.确切时间点
    private LocalDateTime publishTimeOfDate; //日期

    private LocalDateTime publishTimeOfTime; //时间点

    public NoteDto() {
    }

    public NoteDto(LocalDateTime publishTimeOfDate, LocalDateTime publishTimeOfTime) {
        this.publishTimeOfDate = publishTimeOfDate;
        this.publishTimeOfTime = publishTimeOfTime;
    }

    /**
     * 获取
     * @return publishTimeOfDate
     */
    public LocalDateTime getPublishTimeOfDate() {
        return publishTimeOfDate;
    }

    /**
     * 设置
     * @param publishTimeOfDate
     */
    public void setPublishTimeOfDate(LocalDateTime publishTimeOfDate) {
        this.publishTimeOfDate = publishTimeOfDate;
    }

    /**
     * 获取
     * @return publishTimeOfTime
     */
    public LocalDateTime getPublishTimeOfTime() {
        return publishTimeOfTime;
    }

    /**
     * 设置
     * @param publishTimeOfTime
     */
    public void setPublishTimeOfTime(LocalDateTime publishTimeOfTime) {
        this.publishTimeOfTime = publishTimeOfTime;
    }

    public String toString() {
        return "NoteDto{publishTimeOfDate = " + publishTimeOfDate + ", publishTimeOfTime = " + publishTimeOfTime + "}";
    }
}
