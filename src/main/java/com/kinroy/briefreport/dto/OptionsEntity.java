package com.kinroy.briefreport.dto;

/**
 * 前端复选框的数据封装实体
 * @author kinroy
 */
public class OptionsEntity {
    private Object value;
    private String label;

    public OptionsEntity() {
    }

    public OptionsEntity(Object value, String label) {
        this.value = value;
        this.label = label;
    }

    /**
     * 获取
     * @return value
     */
    public Object getValue() {
        return value;
    }

    /**
     * 设置
     * @param value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    public String toString() {
        return "OptionsEntity{value = " + value + ", label = " + label + "}";
    }
}
