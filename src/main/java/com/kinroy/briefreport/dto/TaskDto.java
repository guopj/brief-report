package com.kinroy.briefreport.dto;

public class TaskDto {
    private String taskId;
    private String createUserName;
    private String currentTaskHandlerName;
    private String projectName;

    public TaskDto() {
    }

    public TaskDto(String taskId, String createUserName, String currentTaskHandlerName, String projectName) {
        this.taskId = taskId;
        this.createUserName = createUserName;
        this.currentTaskHandlerName = currentTaskHandlerName;
        this.projectName = projectName;
    }

    /**
     * 获取
     * @return taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取
     * @return createUserName
     */
    public String getCreateUserName() {
        return createUserName;
    }

    /**
     * 设置
     * @param createUserName
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    /**
     * 获取
     * @return currentTaskHandlerName
     */
    public String getCurrentTaskHandlerName() {
        return currentTaskHandlerName;
    }

    /**
     * 设置
     * @param currentTaskHandlerName
     */
    public void setCurrentTaskHandlerName(String currentTaskHandlerName) {
        this.currentTaskHandlerName = currentTaskHandlerName;
    }

    /**
     * 获取
     * @return projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置
     * @param projectName
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "TaskDto{taskId = " + taskId + ", createUserName = " + createUserName + ", currentTaskHandlerName = " + currentTaskHandlerName + ", projectName = " + projectName + "}";
    }
}
