package com.kinroy.briefreport.dto;

import java.util.List;

/**
 * 树形控件解析结构
 * @author kinroy
 */
public class TreeData {
    private  String label; //标签
    private List<TreeData> children; //树形结构的子节点

    public TreeData() {
    }

    public TreeData(String label, List<TreeData> children) {
        this.label = label;
        this.children = children;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 获取
     * @return children
     */
    public List<TreeData> getChildren() {
        return children;
    }

    /**
     * 设置
     * @param children
     */
    public void setChildren(List<TreeData> children) {
        this.children = children;
    }

    public String toString() {
        return "TreeData{label = " + label + ", children = " + children + "}";
    }
}
