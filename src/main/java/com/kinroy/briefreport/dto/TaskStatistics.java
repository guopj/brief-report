package com.kinroy.briefreport.dto;

import java.util.Map;

/**
 * 任务统计信息封装类
 *
 * @author kinroy
 */
public class TaskStatistics {

    private Integer myToDoTasksNum;//待办任务数

    private Integer myAllDoneTaskNum;//已完成的所有任务数

    private Integer weekDoneTaskNum;//本周完成的任务数

    private Double personalPerformance;//个人绩效

    private Map<String, Integer> allTaskPriorityNum; //当前用户的完成任务的级别分布

    private Map<String, Integer[]> weekTaskPriorityNum; //当前用户的完成任务的每周级别分布


    public TaskStatistics() {
    }

    public TaskStatistics(Integer myToDoTasksNum, Integer myAllDoneTaskNum, Integer weekDoneTaskNum, Double personalPerformance, Map<String, Integer> allTaskPriorityNum, Map<String, Integer[]> weekTaskPriorityNum) {
        this.myToDoTasksNum = myToDoTasksNum;
        this.myAllDoneTaskNum = myAllDoneTaskNum;
        this.weekDoneTaskNum = weekDoneTaskNum;
        this.personalPerformance = personalPerformance;
        this.allTaskPriorityNum = allTaskPriorityNum;
        this.weekTaskPriorityNum = weekTaskPriorityNum;
    }

    /**
     * 获取
     * @return myToDoTasksNum
     */
    public Integer getMyToDoTasksNum() {
        return myToDoTasksNum;
    }

    /**
     * 设置
     * @param myToDoTasksNum
     */
    public void setMyToDoTasksNum(Integer myToDoTasksNum) {
        this.myToDoTasksNum = myToDoTasksNum;
    }

    /**
     * 获取
     * @return myAllDoneTaskNum
     */
    public Integer getMyAllDoneTaskNum() {
        return myAllDoneTaskNum;
    }

    /**
     * 设置
     * @param myAllDoneTaskNum
     */
    public void setMyAllDoneTaskNum(Integer myAllDoneTaskNum) {
        this.myAllDoneTaskNum = myAllDoneTaskNum;
    }

    /**
     * 获取
     * @return weekDoneTaskNum
     */
    public Integer getWeekDoneTaskNum() {
        return weekDoneTaskNum;
    }

    /**
     * 设置
     * @param weekDoneTaskNum
     */
    public void setWeekDoneTaskNum(Integer weekDoneTaskNum) {
        this.weekDoneTaskNum = weekDoneTaskNum;
    }

    /**
     * 获取
     * @return personalPerformance
     */
    public Double getPersonalPerformance() {
        return personalPerformance;
    }

    /**
     * 设置
     * @param personalPerformance
     */
    public void setPersonalPerformance(Double personalPerformance) {
        this.personalPerformance = personalPerformance;
    }

    /**
     * 获取
     * @return allTaskPriorityNum
     */
    public Map<String, Integer> getAllTaskPriorityNum() {
        return allTaskPriorityNum;
    }

    /**
     * 设置
     * @param allTaskPriorityNum
     */
    public void setAllTaskPriorityNum(Map<String, Integer> allTaskPriorityNum) {
        this.allTaskPriorityNum = allTaskPriorityNum;
    }

    /**
     * 获取
     * @return weekTaskPriorityNum
     */
    public Map<String, Integer[]> getWeekTaskPriorityNum() {
        return weekTaskPriorityNum;
    }

    /**
     * 设置
     * @param weekTaskPriorityNum
     */
    public void setWeekTaskPriorityNum(Map<String, Integer[]> weekTaskPriorityNum) {
        this.weekTaskPriorityNum = weekTaskPriorityNum;
    }

    @Override
    public String toString() {
        return "TaskStatistics{myToDoTasksNum = " + myToDoTasksNum + ", myAllDoneTaskNum = " + myAllDoneTaskNum + ", weekDoneTaskNum = " + weekDoneTaskNum + ", personalPerformance = " + personalPerformance + ", allTaskPriorityNum = " + allTaskPriorityNum + ", weekTaskPriorityNum = " + weekTaskPriorityNum + "}";
    }
}
