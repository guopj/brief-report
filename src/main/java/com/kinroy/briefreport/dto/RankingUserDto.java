package com.kinroy.briefreport.dto;

/**
 *  打工人排行榜展示数据实体
 * @author kinroy
 */
public class RankingUserDto {
    private Long uid; //用户id
    private String infoName; //用户姓名
    private int numOfCompleted; //完成任务总数
    private String projectName; //项目名称
    private Double performance; //个人绩效

    public RankingUserDto() {
    }


    public RankingUserDto(Long uid, String infoName, int numOfCompleted, String projectName, Double performance) {
        this.uid = uid;
        this.infoName = infoName;
        this.numOfCompleted = numOfCompleted;
        this.projectName = projectName;
        this.performance = performance;
    }

    /**
     * 获取
     * @return uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置
     * @param uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取
     * @return infoName
     */
    public String getInfoName() {
        return infoName;
    }

    /**
     * 设置
     * @param infoName
     */
    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    /**
     * 获取
     * @return numOfCompleted
     */
    public int getNumOfCompleted() {
        return numOfCompleted;
    }

    /**
     * 设置
     * @param numOfCompleted
     */
    public void setNumOfCompleted(int numOfCompleted) {
        this.numOfCompleted = numOfCompleted;
    }

    /**
     * 获取
     * @return projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置
     * @param projectName
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * 获取
     * @return performance
     */
    public Double getPerformance() {
        return performance;
    }

    /**
     * 设置
     * @param performance
     */
    public void setPerformance(Double performance) {
        this.performance = performance;
    }

    public String toString() {
        return "RankingUserDto{uid = " + uid + ", infoName = " + infoName + ", numOfCompleted = " + numOfCompleted + ", projectName = " + projectName + ", performance = " + performance + "}";
    }
}
