package com.kinroy.briefreport.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Boolean successFlag;
    private String errorMsg;
    private Object data;
    private Long total;
    private String code; //状态码

    public static Result ok() {
        return new Result(true, null, null, null, null);
    }

    public static Result ok(Object data) {
        return new Result(true, null, data, null, null);
    }

    public static Result ok(List<?> data, Long total, String code) {
        return new Result(true, null, data, total, code);
    }

    public static Result fail(String errorMsg) {
        return new Result(false, errorMsg, null, null, null);
    }

    public static Result fail(String errorMsg, String code) {
        return new Result(false, errorMsg, null, null, code);
    }
}

