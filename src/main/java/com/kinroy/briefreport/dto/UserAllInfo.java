package com.kinroy.briefreport.dto;

/**
 * 用户个人信息dto
 *
 * @author kinroy
 */
public class UserAllInfo {
    private Long uid;
    private String name; //账号
    private String phone; //电话
    private String email; //邮箱
    private String gender; //性别
    private String avatar_image; //头像url
    private String introduction; //个人介绍
    private String infoName; //用户真实姓名
    private String roleName; //职位名称
    private String depName; //部门名称
    private String jobType; //职位（开发，测试，交付，实施）


    public UserAllInfo() {
    }

    public UserAllInfo(Long uid, String name, String phone, String email, String gender, String avatar_image, String introduction, String infoName, String roleName, String depName, String JobType) {
        this.uid = uid;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
        this.avatar_image = avatar_image;
        this.introduction = introduction;
        this.infoName = infoName;
        this.roleName = roleName;
        this.depName = depName;
        this.jobType = JobType;
    }

    /**
     * 获取
     * @return uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置
     * @param uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取
     * @return gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 获取
     * @return avatar_image
     */
    public String getAvatar_image() {
        return avatar_image;
    }

    /**
     * 设置
     * @param avatar_image
     */
    public void setAvatar_image(String avatar_image) {
        this.avatar_image = avatar_image;
    }

    /**
     * 获取
     * @return introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置
     * @param introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取
     * @return infoName
     */
    public String getInfoName() {
        return infoName;
    }

    /**
     * 设置
     * @param infoName
     */
    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    /**
     * 获取
     * @return roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置
     * @param roleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取
     * @return depName
     */
    public String getDepName() {
        return depName;
    }

    /**
     * 设置
     * @param depName
     */
    public void setDepName(String depName) {
        this.depName = depName;
    }

    /**
     * 获取
     * @return JobType
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * 设置
     * @param JobType
     */
    public void setJobType(String JobType) {
        this.jobType = JobType;
    }

    public String toString() {
        return "UserAllInfo{uid = " + uid + ", name = " + name + ", phone = " + phone + ", email = " + email + ", gender = " + gender + ", avatar_image = " + avatar_image + ", introduction = " + introduction + ", infoName = " + infoName + ", roleName = " + roleName + ", depName = " + depName + ", JobType = " + jobType + "}";
    }
}
