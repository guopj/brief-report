package com.kinroy.briefreport.dto;

public class TaskAssignDto {
    private String taskId;
    private Long taskHandlerId;


    public TaskAssignDto() {
    }

    public TaskAssignDto(String taskId, Long taskHandlerId) {
        this.taskId = taskId;
        this.taskHandlerId = taskHandlerId;
    }

    /**
     * 获取
     * @return taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取
     * @return taskHandlerId
     */
    public Long getTaskHandlerId() {
        return taskHandlerId;
    }

    /**
     * 设置
     * @param taskHandlerId
     */
    public void setTaskHandlerId(Long taskHandlerId) {
        this.taskHandlerId = taskHandlerId;
    }

    public String toString() {
        return "TaskAssignDto{taskId = " + taskId + ", taskHandlerId = " + taskHandlerId + "}";
    }
}
