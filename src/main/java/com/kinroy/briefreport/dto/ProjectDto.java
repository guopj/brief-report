package com.kinroy.briefreport.dto;


/** project的dto
 * @author kinroy
 */
public class ProjectDto {
    private String projectId;//项目id
    private String projectName;//项目名称


    public ProjectDto() {
    }

    public ProjectDto(String projectId, String projectName) {
        this.projectId = projectId;
        this.projectName = projectName;
    }

    /**
     * 获取
     * @return projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 设置
     * @param projectId
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * 获取
     * @return projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置
     * @param projectName
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String toString() {
        return "ProjectDto{projectId = " + projectId + ", projectName = " + projectName + "}";
    }
}
