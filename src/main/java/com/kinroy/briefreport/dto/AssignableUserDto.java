package com.kinroy.briefreport.dto;

/**
 * 可被分配任务的用户实体
 * @author kinroy
 * 这个dto为分配任务时的展示的用户数据实体
 */
public class AssignableUserDto {
   private Long userId; //用户id
   private int progressTasksNum; //待办任务数
   private String infoName; //用户真实姓名
   private String email; //邮箱

   public AssignableUserDto() {
   }

   public AssignableUserDto(Long userId, int progressTasksNum, String infoName, String email) {
      this.userId = userId;
      this.progressTasksNum = progressTasksNum;
      this.infoName = infoName;
      this.email = email;
   }

   /**
    * 获取
    * @return userId
    */
   public Long getUserId() {
      return userId;
   }

   /**
    * 设置
    * @param userId
    */
   public void setUserId(Long userId) {
      this.userId = userId;
   }

   /**
    * 获取
    * @return progressTasksNum
    */
   public int getProgressTasksNum() {
      return progressTasksNum;
   }

   /**
    * 设置
    * @param progressTasksNum
    */
   public void setProgressTasksNum(int progressTasksNum) {
      this.progressTasksNum = progressTasksNum;
   }

   /**
    * 获取
    * @return infoName
    */
   public String getInfoName() {
      return infoName;
   }

   /**
    * 设置
    * @param infoName
    */
   public void setInfoName(String infoName) {
      this.infoName = infoName;
   }

   /**
    * 获取
    * @return email
    */
   public String getEmail() {
      return email;
   }

   /**
    * 设置
    * @param email
    */
   public void setEmail(String email) {
      this.email = email;
   }

   @Override
   public String toString() {
      return "assignableUserDto{userId = " + userId + ", progressTasksNum = " + progressTasksNum + ", infoName = " + infoName + ", email = " + email + "}";
   }
}
