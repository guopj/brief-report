package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.HandOverNode;
import com.kinroy.briefreport.entity.Project;
import com.kinroy.briefreport.entity.Task;
import org.apache.ibatis.annotations.Param;

public interface IHandOverNodeService extends IService<HandOverNode> {
    Boolean createNode(Task task);

    Result getHandOverNodePath(String taskId);

    Boolean removeAllNodeByTaskId(String taskId);

}
