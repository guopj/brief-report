package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Task;

import java.util.Map;

public interface ITaskService extends IService<Task> {
    Result getMyTasks(Long currentTaskHandlerId);

    Result getTaskById(String taskId);

    Result translationOfTaskInformation(Task task);

    Result editTaskHandlerUser(String taskId, Long changerHandlerUserId);

    Result submitTask(Task task);

    Result taskRepulse(Task task);

    Result editTask(Task task);

    Double getPerformance(Long userId);

    Map<String,Integer> getObtainDifferentTaskTypes(Long userId);

    Map<String,Integer> getObtainDifferentTaskPriority(Long userId);

    Result getTaskStatistics(Long userId);

    Result getUserAllTaskByPage(Long userId,Long currentPage,Long pageSize,String taskStatus);

    Result getTaskConditionalQueryByPage(Long userId,Long currentPage,Long pageSize,String taskStatus,String projectSelectValue,String taskTypeSelectValue,String taskPrioritySelectValue);

    Result selectTaskByIdOrName(String selectValue, Long userId,Long currentPage,Long pageSize,String taskStatus,String projectSelectValue,String taskTypeSelectValue,String taskPrioritySelectValue);

    Result getTaskConditionalQueryByPageForManager(Long userId,Long currentPage,Long pageSize,String taskStatus,String projectSelectValue,String taskTypeSelectValue,String taskPrioritySelectValue);

    Result addTask(Long createUserId,Task task);

    Result delTask(String TaskId);
}
