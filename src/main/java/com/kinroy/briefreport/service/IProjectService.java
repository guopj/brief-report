package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.ProjectInfoDto;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Project;
import com.kinroy.briefreport.entity.User;

import java.util.List;

public interface IProjectService extends IService<Project> {
    Result getProjectNameById(String projectId);

    List<Long> getUserListByProjectId(String projectId);

    Result getMyManagerProjects(Long userId);

    Result getProjectInfoById (String projectId);

    Result addProjectMembers(String projectId,List<Long> addUsersIds);

    Result removeFormProject(String projectId,Long userId);

    Result getProjectConditionalQueryByPage (Long currentPage, Long pageSize,String selectValue,Integer projectStatusSelectValue);

    Result delProject(String projectId,Long userId);

    Result editProjectInfo(ProjectInfoDto projectInfoDto);

    Result projectAssignment(ProjectInfoDto projectInfoDto, Long userId,Long projectLeaderId);

    Result selectProjectByIdOrName(String selectValue);

    Result addProject(Project project,Long userId);
}
