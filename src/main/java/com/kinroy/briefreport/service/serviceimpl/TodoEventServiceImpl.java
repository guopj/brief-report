package com.kinroy.briefreport.service.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.TodoEvent;
import com.kinroy.briefreport.mapper.TodoEventMapper;
import com.kinroy.briefreport.service.ITodoEventService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author kinroy
 * @create 2024-05-30  15:18
 * @Description
 */
@Service
public class TodoEventServiceImpl extends ServiceImpl<TodoEventMapper, TodoEvent> implements ITodoEventService {

    @Autowired
    private TodoEventMapper eventMapper;

    /**
     * 新增待办事务
     * @param event
     * @return
     */
    @Override
    public Result addNewTodoEvent(TodoEvent event) {
        int insert = eventMapper.insert(event);
        if (insert>0) {
            return Result.ok();
        }
        return Result.fail("添加失败！");
    }

    /**
     * 根据代办事务状态查询某一用户的代办事务
     * @param userId
     * @param eventStatus
     * @return
     */
    @Override
    public Result getUserTodoEventByStatus(Long userId, Integer eventStatus) {
        // 构建查询条件
        LambdaQueryWrapper<TodoEvent> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(userId!=null||userId!=0L,TodoEvent::getUserId,userId);
        wrapper.eq(eventStatus!=null,TodoEvent::getEventStatus,eventStatus);
        // 查询满足条件的数据
        List<TodoEvent> todoEvents = eventMapper.selectList(wrapper);
        return Result.ok(todoEvents);
    }

    /**
     * 修改代办事务
     * @param eventList
     * @return
     */
    @Override
    public Result updateTodoEvent(List<TodoEvent> eventList) {
        try {
            Boolean aBoolean = eventMapper.doneTodoEvent(eventList);
            if (aBoolean) {
                return Result.ok();
            } else {
                return Result.fail("批量完成待办事务失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail("批量完成待办事务失败！");
    }

    /**
     * 删除待办事务
     * @param eventList
     * @return
     */
    @Override
    public Result delTodoEvent(List<TodoEvent> eventList) {
        try {
            Boolean aBoolean = eventMapper.delTodoEvent(eventList);
            if (aBoolean) {
                return Result.ok();
            } else {
                return Result.fail("批量删除待办事务失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail("批量删除待办事务失败！");
    }
}
