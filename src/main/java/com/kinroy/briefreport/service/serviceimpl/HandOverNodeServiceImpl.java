package com.kinroy.briefreport.service.serviceimpl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.HandOverNode;
import com.kinroy.briefreport.entity.Task;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.entity.UserInfo;
import com.kinroy.briefreport.mapper.HandOverNodeMapper;
import com.kinroy.briefreport.mapper.UserMapper;
import com.kinroy.briefreport.service.IHandOverNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class HandOverNodeServiceImpl extends ServiceImpl<HandOverNodeMapper, HandOverNode> implements IHandOverNodeService {
    @Autowired
    private HandOverNodeMapper nodeMapper;

    @Autowired
    private UserMapper userMapper;
    /**
     * 传入要创建任务流转节点的任务实体
     * 便可创建任务当前状态的任务流转节点
     *
     * @param task
     * @return true:创建节点成功   false：创建节点失败
     */
    @Override
    public Boolean createNode(Task task) {
        if (task == null) {
            return false;
        }
        HandOverNode node = new HandOverNode();
        //封装任务基本信息
        node.setTaskStatus(task.getTaskStatus());
        node.setTaskHandlerId(task.getCurrentTaskHandlerId());
        node.setTaskId(task.getTaskId());
        //查出最新节点
        HandOverNode latestNode = nodeMapper.getLatestNode(task);
        //测试代码回滚
        User userTest = new User();
        User userTest1 = new User();



        User user = new User();
        if (latestNode == null) {
            //没有上一个节点，表示当前任务为”创建未处理“，所有为此创建第一个任务节点
            node.setHandOverNum(1);
            //todo： 创建新节点报错失效，修复一下
            //kinroy 2023.12.18 为了让流转路径显示经办人姓名，在这里再查询一下一下经办人姓名封装一下
            user.setUid(task.getCurrentTaskHandlerId());
            UserInfo userInfo = userMapper.getUserInfo(user);
            node.setTaskHandlerName(userInfo.getInfoName());

            Boolean save = nodeMapper.saveNode(node);
            /*boolean save = save(node);*/
            //todo:kinroy 2024.1.21 添加新的功能，就是在添加新的任务节点时，同步更一下一下task表中对应的task的taskHandOverPath
            return save;
        }
        //todo :在这里报错了，解决一下
        int handOverNum = latestNode.getHandOverNum();
        node.setHandOverNum((handOverNum + 1));
        /* boolean save = save(node);*/

        //kinroy 2023.12.18 为了让流转路径显示经办人姓名，在这里再查询一下一下经办人姓名封装一下
        user.setUid(task.getCurrentTaskHandlerId());
        UserInfo userInfo = userMapper.getUserInfo(user);
        node.setTaskHandlerName(userInfo.getInfoName());

        //todo：savenode报错，修改一下
        Boolean save = nodeMapper.saveNode(node);

        return save;
    }


    /**
     * 根据taskId获取到当前的task的所有任务节点
     * 并且根据handover_num升序排序返回
     *
     * @param taskId
     * @return
     */
    @Override
    public Result getHandOverNodePath(String taskId) {
        if (StrUtil.isBlank(taskId)) {
            return Result.fail("获取任务流转节点失败！");
        }
        //将获取到的任务流转节点进行排序,使用sql的asc升序
        List<HandOverNode> handOverNodePath = nodeMapper.getHandOverNodePath(taskId);
        if (handOverNodePath != null) {
            return Result.ok(handOverNodePath);
        }
        return Result.fail("获取任务流转节点失败");
    }

    /**
     * 删除当前taskId相关的所有任务节点
     * @param taskId
     * @return
     */
    @Override
    public Boolean removeAllNodeByTaskId(String taskId) {
        Boolean aBoolean = nodeMapper.removeAllNodeByTaskId(taskId);
        return aBoolean;
    }
}
