package com.kinroy.briefreport.service.serviceimpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.TreeData;
import com.kinroy.briefreport.dto.UserAllInfo;
import com.kinroy.briefreport.entity.Department;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.entity.UserInfo;
import com.kinroy.briefreport.enums.role.UserRole;
import com.kinroy.briefreport.mapper.DepartmentMapper;
import com.kinroy.briefreport.mapper.UserMapper;
import com.kinroy.briefreport.service.IDepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {


    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 获取部门信息，并封装成树形空间能解析的数据结构 TreeData
     *
     * @param userRoleNum
     * @return
     */
    @Override
    public Result getDepartment(Integer userRoleNum) {
        //1.判断的用户的权限
        if (userRoleNum != UserRole.DEPARTMENT_LEADERS.getCode()) {
            return Result.fail("您没有这个权限！");
        }
        List<Department> departmentList = departmentMapper.getDepartmentList();
        //2.封装成为树形数据
        TreeData treeFatherData = new TreeData();
        treeFatherData.setLabel("江西理工大学-大数据202班");
        List<TreeData> treeFatherChildrenList = new ArrayList<>();
        treeFatherData.setChildren(treeFatherChildrenList);
        for (Department department : departmentList) {
            String departmentName = department.getDepartmentName();
            TreeData treeChildrenData = new TreeData();
            treeChildrenData.setLabel(departmentName);
            //前端显示的时候，对应项估计不能为null
            List<TreeData> list=new ArrayList<>();
            treeChildrenData.setChildren(list);
            treeFatherChildrenList.add(treeChildrenData);
        }
        return Result.ok(treeFatherData);
    }

    /**
     * 获取当前userId所管理的部门的所有成员信息
     * @param currentUserId
     * @return
     */
    @Override
    public Result getDepMemberList(Long currentUserId) {
        //1.判断当前用户权限
        int userRole = userMapper.getUserRoles(currentUserId);
        if (userRole!=1){
            return Result.fail("当前用户没有权限进行此操作！");
        }
        //2.获取当前用户所管理的部门id
        Long departmentIdByUserId = departmentMapper.getDepartmentIdByUserId(currentUserId);
        List<Long> allUserInDepByDepId = departmentMapper.getAllUserInDepByDepId(departmentIdByUserId, currentUserId);
        
        //3.封装用户信息并返回前端展示
        List<UserAllInfo> userAllInfoList = new ArrayList<>();
        for (int i = 0; i < allUserInDepByDepId.size(); i++) {
            Long userId = allUserInDepByDepId.get(i);
            UserAllInfo userAllInfo = new UserAllInfo();
            //0.获取用户电话、账号信息
            User byId = userMapper.getUserByUserId(userId);
            BeanUtils.copyProperties(byId, userAllInfo);

            //1.查询user_info表 获取用户的 真实姓名、头像url、邮箱、性别、个人简介
            UserInfo userInfo = userMapper.getUserInfo(byId);
            BeanUtils.copyProperties(userInfo, userAllInfo);
            //kinroy 2023.12.30这里没把图片url封装上
            String userImgUrl = userMapper.getUserImgUrl(userId);
            userAllInfo.setAvatar_image(userImgUrl);
            //2.查询tb_department表和 department_user_relation联表，获取用户的所在的部门名称
            //todo:联表查询出现问题了
            String departmentName = departmentMapper.getDepartmentNameByUserId(userId);
            userAllInfo.setDepName(departmentName);

            //3.查询出当前用户的职位序号
            int userRoles = userMapper.getUserRoles(userId);
            switch (userRoles) {
                case 1:
                    //部门领导
                    userAllInfo.setRoleName(departmentName + "-" + "部门领导");
                    break;
                case 2:
                    //项目组长
                    userAllInfo.setRoleName(departmentName + "-" + "项目组长");
                    break;
                case 3:
                    //普通员工
                    userAllInfo.setRoleName(departmentName + "-" + "员工");
                    break;
            }
            userAllInfoList.add(userAllInfo);
        }
        return Result.ok(userAllInfoList);
    }
}
