package com.kinroy.briefreport.service.serviceimpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.RemindMessage;
import com.kinroy.briefreport.mapper.RemindMsgMapper;
import com.kinroy.briefreport.service.IRemindMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RemindMsgServiceImpl extends ServiceImpl<RemindMsgMapper, RemindMessage> implements IRemindMsgService {


    @Autowired
    private RemindMsgMapper remindMsgMapper;

    /**
     * 获取当前用户的未读信息
     *
     * @param userId
     * @return
     */
    @Override
    public Result getMyRemindMsgs(Long userId) {
        List<RemindMessage> myUnReadRemindMsg = remindMsgMapper.getMyUnReadRemindMsg(userId);
        return Result.ok(myUnReadRemindMsg);
    }

    /**
     * 添加提醒
     *
     * @param remindMessage
     * @return
     */
    @Override
    public Boolean addRemindMsg(RemindMessage remindMessage) {
        int insert = remindMsgMapper.insert(remindMessage);
        if (insert > 0) {
            return true;
        }
        return false;
    }

    /**
     * 单条提醒已读功能
     * 将当前提醒的状态改成 1即可
     *
     * @param tableId
     * @return
     */
    @Override
    public Result readRemind(Integer tableId) {
        if (tableId == null) {
            return Result.fail("已读失败！");
        }
        boolean b = remindMsgMapper.readRemind(tableId);
        if (b) {
            return Result.ok();
        }
        return Result.fail("已读失败！");
    }

    /**
     * 多条消息批量已读功能
     *
     * @param remindMessageList
     * @return
     */
    @Override
    public Result readAllRemind(List<RemindMessage> remindMessageList) {
        try {
            Boolean aBoolean = remindMsgMapper.readAllRemind(remindMessageList);
            if (aBoolean) {
                return Result.ok();
            } else {
                return Result.fail("批量已读失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail("批量已读失败！");
    }

}
