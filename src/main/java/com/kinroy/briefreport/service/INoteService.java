package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Note;
import com.kinroy.briefreport.service.serviceimpl.NoteServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

public interface INoteService extends IService<Note> {
    Result publishNote(Note note) throws Exception;

    Result getNoteConditionalQueryByPage(Long publishUserid, Long currentPage, Long pageSize,
                                         Integer noteStatusSelectValue, Integer noteTypeSelectValue,
                                         LocalDateTime publishTime,Integer queryType,
                                         String noteNameSelectValue);

    Result getNoteAllInfoByNoteId(String noteId);

    Result editNoteByNoteId(Note note);

    Result delNoteByNoteId(String noteId,Integer role) throws Exception;

    Result getMyUnSubmitNotes(Long publishUserid);

    Result getNoteById(String noteId);

    Result getNoteDtoToShow (Long userId,String noteTypeName);

    Integer getUnPublishNoteNumByType(Integer noteType,Long publishUserId);
}
