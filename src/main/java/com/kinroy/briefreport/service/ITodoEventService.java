package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Department;
import com.kinroy.briefreport.entity.TodoEvent;

import java.util.List;

public interface ITodoEventService extends IService<TodoEvent> {

    Result addNewTodoEvent(TodoEvent event);

    Result getUserTodoEventByStatus(Long userId,Integer eventStatus);

    Result updateTodoEvent(List<TodoEvent> eventList);

    Result delTodoEvent(List<TodoEvent> eventList);

}
