package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Department;

public interface IDepartmentService extends IService<Department> {
    Result getDepartment(Integer userRoleNum);

    Result getDepMemberList(Long currentUserId);
}
