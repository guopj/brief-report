package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.UserAllInfo;
import com.kinroy.briefreport.dto.UserDto;
import com.kinroy.briefreport.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kinroy
 */
public interface IUserService extends IService<User> {
    public Boolean fastRegister(User user);

    Boolean verifyPermissions(User user);

    Result getAssignableUsers(User user, String projectId);

    Result getUserRoles(Long userId);

    Result uploadAvatar(MultipartFile multipartFile, HttpServletRequest request);

    void downloadAvatar(UserDto userDto);

    Result getUserAllInfo(Long userId);

    Result editUserAllInfo(UserAllInfo userAllInfo);

    String getAvatar(Long userId);

    Result getEmployeeRanking(String projectId, Long mangerUserId);

    Result getProjectEmployees(String projectId, User user);

    Result getCanAddedEmployees(String projectId);

    Result getUserForManagerConditional(String depName, Integer userRole, Integer currentPage,Integer pageSize);

    Result delUserAllData(Long userId);

    Result addUser(UserAllInfo userAllInfo);

    Result selectUserConditional(String selectValue);

    Result getAllProjectLeader(Long currentProjectLeaderId);

    Result getUserRole(Long userId);

    Result updateUserRole(Long updateUserId, Long currentUserId,Integer updateRoleNum);

    Result getUserJobDetails(Long userId);
}
