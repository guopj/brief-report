package com.kinroy.briefreport.service;

import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.UserDto;

public interface IRegisterService {
    Result register(UserDto userDto);

    Boolean fastRegister(String phoneNum);

    Boolean checkUserNameOrPhone(UserDto userDto);
}
