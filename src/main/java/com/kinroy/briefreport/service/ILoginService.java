package com.kinroy.briefreport.service;

import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.UserDto;
import com.kinroy.briefreport.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

public interface ILoginService {
    Result login(UserDto userDto, boolean rememberMe, HttpSession session);
    String sendVerificationCode(String phoneNum);
}
