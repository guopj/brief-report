package com.kinroy.briefreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.RemindMessage;

import java.util.List;


public interface IRemindMsgService extends IService<RemindMessage> {

    Result getMyRemindMsgs(Long userId);

    Boolean addRemindMsg(RemindMessage remindMessage);

    Result readRemind(Integer tableId);

    Result readAllRemind(List<RemindMessage> remindMessageList);
}
