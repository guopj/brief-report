package com.kinroy.briefreport.service;

public interface ScheduledService {
    void getNoteTaskToExecuteAndUpdateTaskQueue();

    Integer toDoTasksNumRemind(Long userId);

    void dailyNoteRemind();

    void weeklyNoteRemind();

    void MonthlyNoteRemind();

    void delReadRemindMsg();
}
