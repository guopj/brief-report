package com.kinroy.briefreport.exception;

import org.springframework.stereotype.Component;

/**
 * 自定义的异常类
 * @author kinroy
 */
public class ServiceException extends RuntimeException{
    //状态码
    private String code;


    public ServiceException() {
    }

    public ServiceException(String code,String msg) {
        super(msg);
        this.code = code;
    }


    public ServiceException(String msg) {
        super(msg);
        //设置一个code的默认值
        this.code="500";
    }
    /**
     * 获取
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ServiceException{code = " + code + "}";
    }
}
