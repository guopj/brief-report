package com.kinroy.briefreport.exception;

import com.kinroy.briefreport.dto.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 * @author kinroy
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    //在全局异常处理器中捕获我们指定的异常处理办法

    /**
     * 捕获项目的中serviceException异常并处理
     * @param e
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public Result serviceExceptionHandler(ServiceException e){
        return Result.fail(e.getMessage(),e.getCode());
    }
}
