package com.kinroy.briefreport.controller;

import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.HandOverNode;
import com.kinroy.briefreport.service.IHandOverNodeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/handOverNode")
public class HandOverNodeController {

    @Autowired
    private IHandOverNodeService nodeService;
    /**
     * 根据taskId获取到当前的task的所有任务节点
     * 并且根据handover_num排序返回
     * @param taskId
     * @return
     */
    @GetMapping("/getHandOverNodePath")
    public Result getHandOverNodePath(@Param("taskId") String taskId){
       return nodeService.getHandOverNodePath(taskId);
    }


}
