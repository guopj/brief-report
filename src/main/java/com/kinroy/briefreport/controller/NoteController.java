package com.kinroy.briefreport.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.kinroy.briefreport.dto.NoteDto;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Note;
import com.kinroy.briefreport.service.INoteService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author kinroy
 */
@RestController
@RequestMapping("/note")
public class NoteController {

    @Autowired
    private INoteService noteService;

    /**
     * 日志发布功能
     *
     * @param noteDto
     * @return
     */
    @PostMapping("/publishNote")
    public Result publishNote(@RequestBody NoteDto noteDto) throws Exception {
        //kinroy 2024.1.29 发布或保存日志功能前端的数据处理
        Note note = new Note();
        //1.对必要数据进行非空判断 （日志类型、日志名称、日志内容）
        if ((noteDto.getNoteContent() == null || noteDto.getNoteContent() == "")
                || (noteDto.getNoteType() == null)
                || (noteDto.getNoteName() == null || noteDto.getNoteName() == "")) {
            return Result.fail("日志类型、日志名称、日志内容均为必填项！");
        }
        //2.对前端传来的两个日期进行合并处理
        LocalDateTime date = noteDto.getPublishTimeOfDate();
        LocalDateTime time = noteDto.getPublishTimeOfTime();

        // 将日期和时间合并为一个 LocalDateTime 对象
        if (date!=null&&time!=null) {
            LocalDateTime combinedDateTime = LocalDateTime.of(date.toLocalDate(), time.toLocalTime());
            LocalDateTime plusDays = combinedDateTime.plusDays(1);
            LocalDateTime beijingTime = plusDays.plusHours(8);
            noteDto.setPublishTime(beijingTime);
        }
        //3.将noteDto数据copy到note实体中
        BeanUtil.copyProperties(noteDto,note);
        return noteService.publishNote(note);
    }


    /**
     * 获取我当前publishUserid对应用户的日志
     * 通过条件查询获取并且分页返回
     *
     * @param publishUserid
     * @param currentPage
     * @param pageSize
     * @param noteStatusSelectValue
     * @param noteTypeSelectValue
     * @param queryType
     * @return
     */
    @GetMapping("/getNoteConditionalQueryByPage")
    public Result getNoteConditionalQueryByPage(@RequestParam("publishUserid") Long publishUserid,
                                                @RequestParam("currentPage") Long currentPage,
                                                @RequestParam("pageSize") Long pageSize,
                                                @RequestParam("noteStatusSelectValue") Integer noteStatusSelectValue,
                                                @RequestParam("noteTypeSelectValue") Integer noteTypeSelectValue,
                                                @RequestParam("publishTime") String publishTimeString,
                                                @RequestParam("queryType") Integer queryType,
                                                @RequestParam("noteNameSelectValue") String noteNameSelectValue

    ) {
        //1.进行日期转换
        // 将字符串解析为 Instant
        LocalDateTime publishTime = null;
        if (StrUtil.isNotBlank(publishTimeString)) {
            // 将字符串解析为 Instant
            Instant instant = Instant.parse(publishTimeString);
            // 将 Instant 转换为 LocalDateTime
            publishTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        }

        /*if (noteStatusSelectValue==3){
            noteStatusSelectValue=null;
        }*/
        return noteService.getNoteConditionalQueryByPage(publishUserid, currentPage, pageSize, noteStatusSelectValue, noteTypeSelectValue, publishTime, queryType, noteNameSelectValue);
    }


    /**
     * 根据前端选中的noteId，返回相应的note数据
     * 并且返回与其相互绑定的图片数据
     *
     * @param noteId
     * @return
     */
    @GetMapping("/getNoteAllInfoByNoteId")
    public Result getNoteAllInfoByNoteId(@RequestParam("noteId") String noteId) {
        return noteService.getNoteAllInfoByNoteId(noteId);
    }


    /**
     * 前端传来note的表单数据，进行note的内容修改
     * 可修改的前提是noteStatus为 “保存未发布”即状态码为 0或是"定时发布"
     *
     * @param note
     * @return
     */
    @PutMapping("/editNoteByNoteId")
    public Result editNoteByNoteId(@RequestBody Note note) {
        return noteService.editNoteByNoteId(note);
    }


    /**
     * 根据noteId去删除对应的note数据
     * 注意：已经发布了的note是不能进行删除的
     *
     * @param noteId
     * @return
     */
    @DeleteMapping("/delNoteByNoteId")
    public Result delNoteByNoteId(@RequestParam("noteId") String noteId, @RequestParam("userRole") Integer userRole) throws Exception {
        return noteService.delNoteByNoteId(noteId, userRole);
    }


    /**
     * 获取当前用户未提交的日志
     *
     * @param publishUserid
     * @return
     */
    @GetMapping("/getMyUnSubmitNote")
    public Result getMyUnSubmitNotes(@RequestParam("publishUserid") Long publishUserid) {
        return noteService.getMyUnSubmitNotes(publishUserid);
    }


    @GetMapping("/getNoteDtoToShow")
    public Result getNoteDtoToShow(@RequestParam("userId")Long userId,@RequestParam("noteTypeName")String noteTypeName){
        return noteService.getNoteDtoToShow(userId,noteTypeName);
    }


    @GetMapping("/getNoteById")
    public Result getNoteById(@RequestParam("noteId") String noteId) {
        return noteService.getNoteById(noteId);
    }

    @PostMapping("/uploadImageToMinio")
    public String uploadImageToMinio(@RequestBody String text) {
        System.out.println(text);
        return null;
    }


    /**
     * 保存日志中图片
     *
     * @param multipartFile
     * @return
     */
    @PostMapping("/publishNoteWithImage")
    public Result publishNoteWithImage(MultipartFile multipartFile) {
        return null;
    }
}
