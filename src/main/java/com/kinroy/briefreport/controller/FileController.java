package com.kinroy.briefreport.controller;

import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.service.IUserService;
import com.kinroy.briefreport.service.serviceimpl.MinIOFileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 文件传输相关的controller
 *
 * @author kinroy
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private IUserService userService;

    /**
     * 用户头像文件上传
     *
     * @param
     * @return
     */
    @PostMapping("/userInfoImgUpload")
    public Result userInfoImgUpload(@RequestParam("file")MultipartFile file, HttpServletRequest request) {
       return userService.uploadAvatar(file,request);
    }

    /**
     * 用户头像文件下载返回显示
     *
     * @param httpServletResponse
     */
    @GetMapping("/userInfoImgDownload")
    public void userInfoImgDownload(HttpServletResponse httpServletResponse) {

    }
}
