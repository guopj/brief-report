package com.kinroy.briefreport.controller;

import com.kinroy.briefreport.dto.OptionsEntity;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Department;
import com.kinroy.briefreport.service.IDepartmentService;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kinroy
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    /**
     * 获取树形结构的department数据
     *
     * @param userRoleNum
     * @return
     */
    @GetMapping("/getDepartment")
    public Result getDepartment(@RequestParam("userRole") String userRoleNum) {
        Integer value = Integer.valueOf(userRoleNum);
        return departmentService.getDepartment(value);
    }

    /**
     * 获取所有部门的可选项
     *
     * @return
     */
    @GetMapping("/getDepartmentOptions")
    public Result getDepartmentOptions() {
        List<Department> list = departmentService.list();
        List<OptionsEntity> optionsEntityLIst = new ArrayList<>();
        for (Department department : list) {
            OptionsEntity option = new OptionsEntity();
            option.setValue(department.getDepartmentId());
            option.setLabel(department.getDepartmentName());
            optionsEntityLIst.add(option);
        }
        return Result.ok(optionsEntityLIst);
    }

    @GetMapping("/getDepMemberList")
    public  Result getDepMemberList(@RequestParam("currentUserId") Long currentUserId){
        return departmentService.getDepMemberList(currentUserId);
    }
}
