package com.kinroy.briefreport.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kinroy.briefreport.dto.ProjectInfoDto;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.Project;
import com.kinroy.briefreport.service.IProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {
    @Autowired
    private IProjectService projectService;

    /**
     * 通过项目Id获取项目信息
     *
     * @param projectId
     * @return
     */
    @GetMapping("/getProjectInfoById")
    public Result getProjectInfoById(@RequestParam("projectId") String projectId) {
        return projectService.getProjectInfoById(projectId);
    }

    /**
     * 获取所有项目
     *
     * @return
     */
    @GetMapping("/getAllProject")
    public Result getAllProject() {
        List<Project> projectList = projectService.list();
        return Result.ok(projectList);
    }

    /**
     * 根据项目id获取项目名称
     *
     * @param projectId
     * @return
     */
    @GetMapping("/getProjectNameById")
    public Result getProjectNameById(@RequestParam("projectId") String projectId) {
        return projectService.getProjectNameById(projectId);
    }


    /**
     * 通过userId获取当前用户所管理的项目
     *
     * @param userId
     * @return
     */
    @GetMapping("/getMyManagerProjects")
    public Result getMyManagerProjects(@RequestParam("userId") Long userId) {
        return projectService.getMyManagerProjects(userId);
    }

    /**
     * 接收前端传来的需要添加入当前projectId对应项目的用户id
     * 在project_user_relation新增一条数据
     *
     * @param addMembersIds
     * @return
     */
    @PostMapping("/addProjectMembers")
    public Result addProjectMembers(@RequestBody List<Long> addMembersIds, @RequestParam("projectId") String projectId) {
        return projectService.addProjectMembers(projectId, addMembersIds);
    }


    /**
     * 将当前用户id对应的用户移除当前projectId对应的项目
     * 在project_user_relation删除一条数据
     *
     * @param projectId
     * @param userId
     * @return
     */
    @DeleteMapping("/removeFormProject")
    public Result removeFormProject(@RequestParam("projectId") String projectId,
                                    @RequestParam("userId") Long userId) {
        return projectService.removeFormProject(projectId, userId);
    }


    /**
     * 条件查询获取到项目数据并分页返回
     *
     * @return
     */
    @GetMapping("/getProjectConditionalQueryByPage")
    public Result getProjectConditionalQueryByPage(@RequestParam("currentPage") Long currentPage,
                                                   @RequestParam("pageSize") Long pageSize,
                                                   @RequestParam("selectValue") String selectValue,
                                                   @RequestParam("projectStatusSelectValue") Integer projectStatusSelectValue) {
        return projectService.getProjectConditionalQueryByPage(currentPage, pageSize, selectValue, projectStatusSelectValue);
    }


    /**
     * 根据projectId删除项目及其相关信息
     * 必须为当前项目的创建者才能进行删除操作
     *
     * @param projectId
     * @return
     */
    @DeleteMapping("/delProject")
    public Result delProject(@RequestParam("projectId") String projectId, @RequestParam("userId") Long userId) {
        return projectService.delProject(projectId, userId);
    }


    /**
     * 修改项目信息（只允许改项目的状态和项目名称）
     *
     * @param projectInfoDto
     * @return
     */
    @PutMapping("/editProjectInfo")
    public Result editProjectInfo(@RequestBody ProjectInfoDto projectInfoDto) {
        return projectService.editProjectInfo(projectInfoDto);
    }

    /**
     * 指派项目负责人
     *
     * @param projectInfoDto
     * @param userId
     * @param projectLeaderId
     * @return
     */
    @PostMapping("/projectAssignment")
    public Result projectAssignment(@RequestBody ProjectInfoDto projectInfoDto,
                                    @RequestParam("userId") Long userId,
                                    @RequestParam("projectLeaderId") Long projectLeaderId) {
        return projectService.projectAssignment(projectInfoDto, userId, projectLeaderId);
    }


    /**
     * 根据项目名称或者项目id进行搜索
     *
     * @param selectValue
     * @return
     */
    @GetMapping("/selectProjectByIdOrName")
    public Result selectProjectByIdOrName(@RequestParam("selectValue") String selectValue) {
        return projectService.selectProjectByIdOrName(selectValue);
    }


    /**
     * 新增项目
     * @param project
     * @return
     */
    @PostMapping("/addProject")
    public Result addProject(@RequestBody Project project,@RequestParam("loginUser")Long userId){
        return projectService.addProject(project, userId);
    }

    //下面接口进行crud测试接口
  /*  @PostMapping("/addproject")
    public Result addTask(@RequestBody Project project) {
        //todo:属性映射出错，待修复
        boolean save = projectService.save(project);
        if (save) {
            return Result.ok("成功！");
        }
        return Result.fail("失败！");
    }
*/
    @GetMapping("/getallproject")
    public Result getAllTesk() {
        List<Project> list = projectService.list();
        if (list != null) {
            return Result.ok(list);
        }
        return Result.fail("失败！");
    }

    @GetMapping("/getone/{projectName}")
    public Result getOneTask(@RequestParam("projectName") String projectName) {
        //测试一下条件查询
        LambdaQueryWrapper<Project> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(projectName != null && !projectName.equals(""), Project::getProjectName, projectName);
        Project one = projectService.getOne(wrapper);
        if (one != null) {
            return Result.ok(one);
        }
        return Result.fail("失败！");
    }


    @PutMapping("/edit")
    public Result edit(@RequestBody Project project) {
        //测试修改
        boolean b = projectService.updateById(project);
        if (b) {
            return Result.ok("成！");
        }
        return Result.fail("失败！");
    }

    @DeleteMapping("/del/{projectid}")
    public Result del(@RequestParam("projectId") String projectId) {
        boolean b = projectService.removeById(projectId);
        if (b) {
            return Result.ok("成功！");
        }
        return Result.fail("失败！");
    }
}
