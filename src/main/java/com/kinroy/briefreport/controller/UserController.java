package com.kinroy.briefreport.controller;

import com.alibaba.fastjson.JSON;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.UserAllInfo;
import com.kinroy.briefreport.dto.UserDto;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.mapper.UserMapper;
import com.kinroy.briefreport.service.IUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;


    @GetMapping("/getalluser")
    public Result getAllUsers() {
        List<User> users = userService.list();
        if (users != null) {
            return Result.ok(users);
        }
        return Result.fail("查询失败！");
    }


    /**
     * 获取可被分配任务的用户们
     *
     * @param user
     * @param projectId
     * @return
     */
    @GetMapping("/getAssignableUsers")
    public Result getAssignableUsers(@RequestParam("loginUser") String user, @RequestParam("projectId") String projectId) {
        UserDto userDto = JSON.parseObject(user, UserDto.class);
        User user2 = new User();
        BeanUtils.copyProperties(userDto, user2);
        return userService.getAssignableUsers(user2, projectId);
    }


    /**
     * 用户鉴权
     *
     * @param user
     * @return
     */
    @PostMapping("/verifyPermissions")
    public Boolean verifyPermissions(@RequestBody User user) {
        Boolean aBoolean = userService.verifyPermissions(user);
        return aBoolean;
    }


    /**
     * 用户个人信息头像获取
     * 返回头像url
     *
     * @return
     */
    @GetMapping("/getAvatar")
    public Result getAvatar(@Param("userId") Long userId) {
        /*kinroy 2023.12.31 之前封装dto的时候，没有考虑到图像
         * 所有在这里新增一个简单的接口给前端获取到当前用户的头像url
         * */
        if (userService.getAvatar(userId) != null) {
            return Result.ok(userService.getAvatar(userId));
        }
        return Result.fail("获取用户头像图片失败！");
    }


    /**
     * 获取个人的所有信息
     *
     * @param userId
     * @return
     */
    @GetMapping("/getUserAllInfo")
    public Result getUserAllInfo(@Param("userId") Long userId) {
        return userService.getUserAllInfo(userId);
    }


    /**
     * 用户个人信息修改
     *
     * @param userAllInfo
     * @return
     */
    @PostMapping("/editUserAllInfo")
    public Result editUserAllInfo(@RequestBody UserAllInfo userAllInfo) {
        return userService.editUserAllInfo(userAllInfo);
    }


    /**
     * 通过项目id获取当前项目完成任务最多的top5员工排行
     *
     * @param projectId
     * @return
     */
    @GetMapping("/getEmployeeRanking")
    public Result getEmployeeRanking(@RequestParam("projectId") String projectId, @RequestParam("userId") Long mangerUserId) {
        return userService.getEmployeeRanking(projectId, mangerUserId);

    }


    /**
     * 获取当前项目的所有组员AllInfo信息
     *
     * @param user
     * @param projectId
     * @return
     */
    @GetMapping("/getProjectEmployees")
    public Result getProjectEmployees(@RequestParam("loginUser") String user, @RequestParam("projectId") String projectId) {
        UserDto userDto = JSON.parseObject(user, UserDto.class);
        User user2 = new User();
        BeanUtils.copyProperties(userDto, user2);
        return userService.getProjectEmployees(projectId, user2);
    }

    /**
     * 获取可以被加入到项目的用户信息
     *
     * @param projectId
     * @return
     */
    @GetMapping("/getCanAddedEmployees")
    public Result getCanAddedEmployees(@RequestParam("projectId") String projectId) {
        return userService.getCanAddedEmployees(projectId);
    }

    /**
     * 员工管理模块的员工数据条件查询表格数据
     *
     * @param depName
     * @param userRole
     * @return
     */
    @GetMapping("/getUserForManagerConditional")
    public Result getUserForManagerConditional(@RequestParam("depName") String depName,
                                               @RequestParam("userRole") Integer userRole,
                                               @RequestParam("currentPage") Integer currentPage,
                                               @RequestParam("pageSize") Integer pageSize
    ) {
        return userService.getUserForManagerConditional(depName, userRole, currentPage, pageSize);
    }


    /**
     * 根据userId删除对应用户的所有信息，如果用户有任务
     * 则会帮当前任务进行挂起
     *
     * @param userId
     * @return
     */
    @DeleteMapping("/delUserAllData")
    public Result delUserAllData(@RequestParam("userId") Long userId) {
        return userService.delUserAllData(userId);
    }


    /**
     * 再员工管理界面进行的新增员工功能
     *
     * @param userAllInfo
     * @return
     */
    @PostMapping("/addUser")
    public Result addUser(@RequestBody UserAllInfo userAllInfo) {
        return userService.addUser(userAllInfo);
    }

    /**
     * 根据UserId或者用户姓名进行条件查询
     *
     * @param selectValue
     * @return
     */
    @GetMapping("/selectUserConditional")
    public Result selectUserConditional(@RequestParam("selectValue") String selectValue) {
        return userService.selectUserConditional(selectValue);
    }

    /**
     * 获取除了currentProjectLeaderId对应的项目组长以外
     * 所有项目组长的信息
     *
     * @param currentProjectLeaderId
     * @return
     */
    @GetMapping("/getAllProjectLeader")
    public Result getAllProjectLeader(@RequestParam("currentProjectLeaderId") Long currentProjectLeaderId) {
        return userService.getAllProjectLeader(currentProjectLeaderId);
    }


    /**
     * 获取用户权限
     *
     * @param userId
     * @return
     */
    @GetMapping("/getUserRole")
    public Result getUserRole(@RequestParam("updateUserId") Long userId) {
        return userService.getUserRole(userId);
    }


    /**
     * 修改用户权限
     *
     * @param updateUserId
     * @param loginUserId
     * @return
     */
    @PutMapping("/updateUserRole")
    public Result updateUserRole(@RequestParam("updateUserId") Long updateUserId,
                                 @RequestParam("loginUserId") Long loginUserId,
                                 @RequestParam("updateRoleNum") Integer updateRoleNum
    ) {
       return userService.updateUserRole(updateUserId,loginUserId,updateRoleNum);
    }

    /**
     * 获取当前用户的日志和任务完成详细情况数据
     * @param userId
     * @return
     */
    @GetMapping("/getUserJobDetails")
    public  Result getUserJobDetails(@RequestParam("userId")Long userId){
        return userService.getUserJobDetails(userId);
    }
}
