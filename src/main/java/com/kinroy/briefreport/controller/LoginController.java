package com.kinroy.briefreport.controller;

import cn.hutool.core.lang.UUID;
import com.kinroy.briefreport.annotation.CalculateRuntime;
import com.kinroy.briefreport.annotation.PrintMethodName;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.UserDto;
import com.kinroy.briefreport.entity.SerlectTest;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.service.ILoginService;
import com.kinroy.briefreport.service.IRegisterService;
import com.kinroy.briefreport.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin //解决前后端端口不一样的跨域问题
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private ILoginService loginService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IRegisterService registerService;


    @CalculateRuntime
    @PrintMethodName
    @PostMapping("/userlogin")
    public Result login(@RequestBody UserDto userDto,
                        @RequestParam(defaultValue = "false") boolean rememberMe,
                        HttpSession session) {
        return loginService.login(userDto, rememberMe, session);
    }

    @PostMapping("/register")
    public Result register(@RequestBody UserDto userDto) {
        return registerService.register(userDto);

    }

    @GetMapping("/sendcode")
    public String sendCode(@RequestParam("phone") String phoneNum) {
        return loginService.sendVerificationCode(phoneNum);
    }


    @GetMapping("/getalluser")
    public List<User> getAllUsers() {
        List<User> users = userService.list();
        return users;
    }

    @PostMapping("/checkUserNameOrPhone")
    public Boolean checkUserNameOrPhone(@RequestBody UserDto userDto) {
        return registerService.checkUserNameOrPhone(userDto);
    }

    /*测试快速注册的接口*/
    @GetMapping("/fasterrigester")
    public Boolean fasterRigester(@RequestParam("phone") String phoneNum) {
        return registerService.fastRegister(phoneNum);
    }

    @GetMapping("/getUserRoles")
    public Result getUserRoles(@RequestParam("userId") Long userId) {
        return userService.getUserRoles(userId);
    }

}
