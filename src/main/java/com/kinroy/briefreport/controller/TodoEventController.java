package com.kinroy.briefreport.controller;

import cn.hutool.db.handler.StringHandler;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.TodoEvent;
import com.kinroy.briefreport.service.ITodoEventService;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author kinroy
 * @create 2024-05-30  15:03
 * @Description
 */
@RestController
@RequestMapping("/todoEvent")
public class TodoEventController {

    @Autowired
    private ITodoEventService eventService;

    /**
     * 添加新的待办事务
     *
     * @param event
     * @return
     */
    @PostMapping("/addNewTodoEvent")
    public Result addNewTodoEvent(@RequestBody TodoEvent event) {
        if (StringHelper.isNullOrEmptyString(event.getContext())) {
            return Result.fail("待办事务不能传空！");
        }
        return eventService.addNewTodoEvent(event);
    }

    /**
     * 删除待办事务
     *
     * @param eventList
     * @return
     */
    @PostMapping("/delTodoEvent")
    public Result delTodoEvent(@RequestBody List<TodoEvent> eventList) {
        if (eventList.isEmpty()) {
            return Result.fail("不能传空！");
        }
        return eventService.delTodoEvent(eventList);
    }

    /**
     * 修改代办事务
     *
     * @param todoEventList
     * @return
     */
    @PutMapping("/updateTodoEvent")
    public Result updateTodoEvent(@RequestBody List<TodoEvent> todoEventList) {
        if (todoEventList.isEmpty()) {
            return Result.fail("不能传空！");
        }
        return eventService.updateTodoEvent(todoEventList);
    }

    /**
     * 根据代办事务状态查询某一用户的代办事务
     *
     * @param userId
     * @param eventStatus
     * @return
     */
    @GetMapping("/getUserTodoEventByStatus")
    public Result getUserTodoEventByStatus(@RequestParam("userId") Long userId,
                                           @RequestParam("eventStatus") Integer eventStatus) {
        return eventService.getUserTodoEventByStatus(userId, eventStatus);
    }
}
