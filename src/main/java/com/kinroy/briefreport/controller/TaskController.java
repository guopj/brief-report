package com.kinroy.briefreport.controller;

import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kinroy.briefreport.config.JsonConfigs;
import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.dto.TaskAssignDto;
import com.kinroy.briefreport.entity.Task;
import com.kinroy.briefreport.service.ITaskService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    @Autowired
    private ITaskService taskService;

    @Resource(name = "dateTimeConfig")
    private JSONConfig dateTimeConfig;

    /**
     * 根据经办人id获取此人的任务
     *
     * @param currentTaskHandlerId
     * @return
     */
    @GetMapping("/getMyTasks")
    public Result getMyTasks(@RequestParam("currentTaskHandlerId") Long currentTaskHandlerId) {
        return taskService.getMyTasks(currentTaskHandlerId);
    }

    /**
     * 通过任务id获取任务信息
     *
     * @param taskId
     * @return
     */
    @GetMapping("/getTaskById")
    public Result getTaskById(@RequestParam("taskId") String taskId) {
        return taskService.getTaskById(taskId);
    }


    @GetMapping("/getTaskByName")
    public Result getTaskByName(@RequestParam("taskName") String taskName) {
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
        //Like模糊查询
        wrapper.like(taskName != null && !taskName.equals(""), Task::getTaskName, taskName);
        Task one = taskService.getOne(wrapper);
        if (one != null) {
            return Result.ok(one);
        }
        return Result.fail("没有该数据");
    }


    /**
     * 讲任务中的创建者、经办人、项目名的id转换成对应名称（连表查询）
     *
     * @param task
     * @return
     */
    @PostMapping("/translationOfTaskInformation")
    public Result translationOfTaskInformation(@RequestBody Task task) {
        return taskService.translationOfTaskInformation(task);
    }

    /**
     * 将当前任务的id和要改成的handlerUserid发到后端，修改当前经办人实现分配
     *
     * @param taskAssignDto
     * @return
     */
    @PutMapping("/editTaskHandlerUser")
    public Result editTaskHandlerUser(@RequestBody TaskAssignDto taskAssignDto) {
        return taskService.editTaskHandlerUser(taskAssignDto.getTaskId(), taskAssignDto.getTaskHandlerId());
    }


    /**
     * 任务状态提交修改
     *
     * @param task
     * @return
     */
    @PutMapping("/submitTask")
    public Result submitTask(@RequestBody Task task) {
        return taskService.submitTask(task);
    }

    /**
     * 任务打回功能
     *
     * @param task
     * @return
     */
    @PostMapping("/taskRepulse")
    public Result taskRepulse(@RequestBody Task task) {
        return taskService.taskRepulse(task);
    }

    /**
     * 任务编辑功能
     *
     * @param task
     * @return
     */
    @PutMapping("/editTaskSubmit")
    public Result editTaskSubmit(@RequestBody Task task) {
        return taskService.editTask(task);
    }


    /**
     * 新增任务功能
     *
     * @param task
     * @param createUserId
     * @return
     */
    @PostMapping("/addTask")
    public Result addTask(@RequestBody Task task, @RequestParam("createUserId") Long createUserId) {
        return taskService.addTask(createUserId, task);
    }


    /**
     * 通过taskId 删除任务
     *
     * @param taskId
     * @return
     */
    @DeleteMapping("/delTask")
    public Result delTask(@RequestParam("taskId") String taskId) {
        return taskService.delTask(taskId);
    }

   /* //下面接口进行crud测试接口
    @PostMapping("/addtask")
    public Result addTask(@RequestBody Task task) {
        //todo:属性映射出错，待修复
        boolean save = taskService.save(task);
        if (save) {
            return Result.ok("成功！");
        }
        return Result.fail("失败！");
    }

    @GetMapping("/getallTask")
    public Result getAllTesk() {
        List<Task> list = taskService.list();
        if (list != null) {
            return Result.ok(list);
        }
        return Result.fail("失败！");
    }

    @GetMapping("/getone")
    public Result getOneTask(@RequestParam("taskName") String taskName) {
        //测试一下条件查询
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(taskName != null && !taskName.equals(""), Task::getTaskName, taskName);
        Task one = taskService.getOne(wrapper);
        if (one != null) {
            return Result.ok(one);
        }
        return Result.fail("失败！");
    }

    @PutMapping("/edit")
    public Result edit(@RequestBody Task task) {
        //测试修改
        boolean b = taskService.updateById(task);
        if (b) {
            return Result.ok("成！");
        }
        return Result.fail("失败！");
    }

    @DeleteMapping("/del")
    public Result del(@RequestParam("taskid") String taskId) {
        boolean b = taskService.removeById(taskId);
        if (b) {
            return Result.ok("成功！");
        }
        return Result.fail("失败！");
    }*/

    /**
     * 通过userId获取到当前用户的绩效水平
     *
     * @param userId
     * @return
     */
    @GetMapping("/getPerformance")
    public Double getPerformance(@RequestParam("userId") Long userId) {
        return taskService.getPerformance(userId);
    }

    /**
     * 获取各种类型的任务数量，返回一个hashMap，key为任务等级，value为任务数量
     *
     * @param userId
     * @return
     */
    @GetMapping("/obtainDifferentTaskTypes")
    public Result getObtainDifferentTaskTypes(@RequestParam("userId") Long userId) {
        return null;
    }

    /**
     * 通过userId，获取当前用户的任务图标信息
     *
     * @param userId
     * @return
     */
    @GetMapping("/gettaskStatistics")
    public Result getTaskStatistics(@RequestParam("userId") Long userId) {
        return taskService.getTaskStatistics(userId);
    }

    /**
     * 任务状态作为条件获取当前用户的所有满足条件的任务
     *
     * @param userId
     * @param currentPage
     * @param pageSize
     * @param taskStatus
     * @return
     */
    @GetMapping("/getUserAllTaskByPage")
    public Result getUserAllTaskByPage(@RequestParam("userId") Long userId,
                                       @RequestParam("currentPage") Long currentPage,
                                       @RequestParam("pageSize") Long pageSize,
                                       @RequestParam("taskStatus") String taskStatus) {
        return taskService.getUserAllTaskByPage(userId, currentPage, pageSize, taskStatus);
    }


    /**
     * 任务汇总的条件查询（查询条件为：1.所属项目 2.任务类别 3.任务优先级 4.任务状态）
     * 同时进行分页
     *
     * @param userId                  当前用户
     * @param currentPage             当前页码
     * @param pageSize                页面大小
     * @param taskStatus              任务状态
     * @param projectSelectValue      所属项目
     * @param taskTypeSelectValue     任务类别
     * @param taskPrioritySelectValue 任务优先级
     * @return
     */
    @GetMapping("/getTaskConditionalQueryByPage")
    public Result getTaskConditionalQueryByPage(@RequestParam("userId") Long userId,
                                                @RequestParam("currentPage") Long currentPage,
                                                @RequestParam("pageSize") Long pageSize,
                                                @RequestParam("taskStatus") String taskStatus,
                                                @RequestParam("projectSelectValue") String projectSelectValue,
                                                @RequestParam("taskTypeSelectValue") String taskTypeSelectValue,
                                                @RequestParam("taskPrioritySelectValue") String taskPrioritySelectValue

    ) {
        return taskService.getTaskConditionalQueryByPage(userId, currentPage, pageSize, taskStatus, projectSelectValue, taskTypeSelectValue, taskPrioritySelectValue);
    }


    /**
     * 通过任务id精确查询任务（一条）、任务名称模糊查询任务（多条匹配需要分页） 并且分页返回
     *
     * @param selectValue
     * @param userId
     * @param currentPage
     * @param pageSize
     * @return
     */
    @GetMapping("/selectTaskByIdOrName")
    public Result selectTaskByIdOrName(@Param("selectValue") String selectValue,
                                       @RequestParam("userId") Long userId,
                                       @RequestParam("currentPage") Long currentPage,
                                       @RequestParam("pageSize") Long pageSize,
                                       @RequestParam("taskStatus") String taskStatus,
                                       @RequestParam("projectSelectValue") String projectSelectValue,
                                       @RequestParam("taskTypeSelectValue") String taskTypeSelectValue,
                                       @RequestParam("taskPrioritySelectValue") String taskPrioritySelectValue) {
        return taskService.selectTaskByIdOrName(selectValue, userId, currentPage, pageSize, taskStatus, projectSelectValue, taskTypeSelectValue, taskPrioritySelectValue);
    }


    /**
     * 项目管理者及以上
     * 任务汇总的条件查询（查询条件为：1.所属项目 2.任务类别 3.任务优先级 4.任务状态）
     * 同时进行分页
     *
     * @param userId                  当前用户
     * @param currentPage             当前页码
     * @param pageSize                页面大小
     * @param taskStatus              任务状态
     * @param projectSelectValue      所属项目
     * @param taskTypeSelectValue     任务类别
     * @param taskPrioritySelectValue 任务优先级
     * @return
     */
    @GetMapping("/getTaskConditionalQueryByPageForManager")
    public Result getTaskConditionalQueryByPageForManager(@RequestParam("userId") Long userId,
                                                          @RequestParam("currentPage") Long currentPage,
                                                          @RequestParam("pageSize") Long pageSize,
                                                          @RequestParam("taskStatus") String taskStatus,
                                                          @RequestParam("projectSelectValue") String projectSelectValue,
                                                          @RequestParam("taskTypeSelectValue") String taskTypeSelectValue,
                                                          @RequestParam("taskPrioritySelectValue") String taskPrioritySelectValue

    ) {
        return taskService.getTaskConditionalQueryByPageForManager(userId, currentPage, pageSize, taskStatus, projectSelectValue, taskTypeSelectValue, taskPrioritySelectValue);
    }

}
