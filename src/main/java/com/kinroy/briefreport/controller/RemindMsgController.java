package com.kinroy.briefreport.controller;

import com.kinroy.briefreport.dto.Result;
import com.kinroy.briefreport.entity.RemindMessage;
import com.kinroy.briefreport.service.IRemindMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/remind")
public class RemindMsgController {

    @Autowired
    private IRemindMsgService remindMsgService;

    /**
     * 获取到当前userId的用户的未读消息提醒
     *
     * @param userId
     * @return
     */
    @GetMapping("/getMyRemind")
    public Result getMyRemind(@RequestParam("userId") Long userId) {
        return remindMsgService.getMyRemindMsgs(userId);
    }

    /**
     * 已读消息
     *
     * @param tableId
     * @return
     */
    @PutMapping("/readRemind")
    public Result readRemind(@RequestParam("tableId") Integer tableId) {
        return remindMsgService.readRemind(tableId);
    }

    /**
     * 批量已读消息
     *
     * @param remindMessageList
     * @return
     */
    @PutMapping("/readAllRemind")
    public Result readAllRemind(@RequestBody List<RemindMessage> remindMessageList) {
        return remindMsgService.readAllRemind(remindMessageList);
    }
}
