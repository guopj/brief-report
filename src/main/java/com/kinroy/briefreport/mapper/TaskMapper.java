package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.dto.TaskDto;
import com.kinroy.briefreport.entity.Task;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Mapper
public interface TaskMapper extends BaseMapper<Task> {
    /**
     * 获取当前用户所有未完成或未通过的任务
     * @param currentTaskHandlerId
     * @return
     */
    List<Task> getMyTasks(@Param("currentTaskHandlerId") Long currentTaskHandlerId);

    /**
     * 解析出当前任务的所属项目名称
     * @param task
     * @return
     */
    String translationOfTaskInformationProjectName(Task task);

    /**
     * 解析出当前任务的创建人和经办人姓名
     * @param userId
     * @return
     */
    String translationOfTaskInformationUserName(@Param("userId") Long userId);

    /**
     * 获取当前用户待办任务数
     * @param currentTaskHandlerId
     * @return
     */
    int countUndoTasks(@Param("currentTaskHandlerId") Long currentTaskHandlerId);


    /**
     * 将要修改经办人的任务id和要改成的用户id传入，进行修改
     * @param taskId
     * @param currentTaskHandlerId
     * @return
     */
    Boolean editTaskHandlerUser(@Param("taskId") String taskId,@Param("currentTaskHandlerId")Long currentTaskHandlerId);

    /**
     * 传入任务实体，对应修改任务实体
     * @param task
     * @return
     */
    Boolean updateTaskStatus(Task task);

    /**
     * 获取当前经办人的完成任务数和任务关闭数和
     * @param currentTaskHandlerId
     * @return
     */
    int countCompletedTasks(@Param("currentTaskHandlerId")Long currentTaskHandlerId);

    /**
     * 获取当前经办人的所有任务数
     * @param currentTaskHandlerId
     * @return
     */
    int countAllTasksFormCurrentUser(@Param("currentTaskHandlerId")Long currentTaskHandlerId);

    /**
     * 根据任务优先级获取相应的完成的任务类型数量
     * @param currentTaskHandlerId
     * @param taskPriority
     * @return
     */
    int  countTaskByPriority(@Param("currentTaskHandlerId")Long currentTaskHandlerId,@Param("taskPriority") int taskPriority);

    /**
     * 根据任务类型获取相应的完成的任务类型数量
     * @param currentTaskHandlerId
     * @param taskType
     * @return
     */
    int  countTaskByType(@Param("currentTaskHandlerId")Long currentTaskHandlerId,@Param("taskType") int taskType);

    /**
     * 获取到该用户本周完成的任务数
     * @param currentTaskHandlerId
     * @param startOfWeek
     * @param endOfWeek
     * @return
     */
    int countTaskByThisWeek(@Param("currentTaskHandlerId")Long currentTaskHandlerId, @Param("startOfWeek") String startOfWeek, @Param("endOfWeek")String endOfWeek);

    /**
     * 获取当前用户这一周完成的所有任务
     * @param currentTaskHandlerId
     * @param startOfWeek
     * @param endOfWeek
     * @return
     */
    List<Task> getTaskListByThisWeek(@Param("currentTaskHandlerId")Long currentTaskHandlerId, @Param("startOfWeek")String startOfWeek, @Param("endOfWeek")String endOfWeek);

    /**
     * 通过userId和projectId获取当前用户在当前项目
     * 中完成的项目数
     * @param currentTaskHandlerId
     * @param projectId
     * @return
     */
    int countDoneTaskNumOfProjectByUserId(@Param("currentTaskHandlerId")Long currentTaskHandlerId,@Param("projectId")String projectId);

    /**
     * 通过projectId获取到当前项目的所有任务的taskId
     * @param projectId
     * @return
     */
    List<String> getTaskIdListByProjectId(@Param("projectId") String projectId);

    /**
     * 根据projectId，统计当前项目未完成的任务数
     * @param projectId
     * @return
     */
    Integer  countUnCompleteNumInProject(@Param("projectId") String projectId);
}
