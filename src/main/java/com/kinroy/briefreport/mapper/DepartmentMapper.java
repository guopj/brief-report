package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.Department;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author kinroy
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<Department> {
    /**
     * 通过userId去查询出当前用户所在的部门名称
     * 查询tb_department 和 department_user_relation 的联表
     * @param UserId
     * @return
     */
    String getDepartmentNameByUserId(@Param("userId") Long UserId );

    /**
     * 获取所有部门信息
     * @return
     */
    List<Department> getDepartmentList();


    /**
     * 修改对应userId用户的部门
     * @param userId
     * @param depId
     * @return
     */
    Boolean updateDepOfUser(@Param("userId") Long userId,@Param("depId") Long depId);

    /**
     * 为当前userId对应的用户绑定部门关系
     * @param userId
     * @param depId
     * @return
     */
    Boolean addDepForUser(@Param("userId") Long userId,@Param("depId") Long depId);


    /**
     * 通过departmentId来获取相应部门的所有员工的id
     * @param DepId
     * @param excludeUserId 需要排除的userId
     * @return
     */
    List<Long> getAllUserInDepByDepId(@Param("depId")Long DepId,@Param("excludeUserId") Long excludeUserId);

    /**
     * 通过userId去查询出当前用户所在的部门Id
     * @param UserId
     * @return
     */
    Long getDepartmentIdByUserId(@Param("userId") Long UserId);

}
