package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.entity.UserInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Insert("insert into tb_user (name,phone,salt,password) values(#{name},#{phone},#{salt},#{password});")
    Boolean fastInsertUser(User user);

    /**
     * 创建新用户时，自动添加默认user详细信息
     *
     * @param userInfo
     * @return
     */
    Boolean createUserInfo(UserInfo userInfo);

    /**
     * 查出user的详细用户信息
     *
     * @param user
     * @return
     */
    UserInfo getUserInfo(User user);


    /**
     * 查询 user_role表，统计能查出的数据数，count>1，则有权限
     *
     * @param user
     * @return
     */
    int verifyPermissions(User user);


    List<UserInfo> getUserInfoList(@Param("userIds") List<Long> userIds);

    /**
     * 通过userid获取当前用户的"最高"的角色序号
     * 1：管理员、部门领导  2：项目组长   3：员工
     *
     * @param userId
     * @return
     */
    int getUserRoles(@Param("userId") Long userId);

    /**
     * 通过userId从user_info表中获取相应的头像url
     *
     * @param userId
     * @return
     */
    String getUserImgUrl(@Param("userId") Long userId);

    /**
     * 通过userId 设置对应user的imgUrl
     *
     * @param userId
     * @param imgUrl
     * @return
     */
    boolean setUserImgUrl(@Param("userId") Long userId, @Param("imgUrl") String imgUrl);


    /**
     * 设置用户角色权限
     *
     * @param userRole
     * @param userId
     * @return
     */
    boolean setUserRole(@Param("userRole") int userRole, @Param("userId") Long userId);

    /**
     * 通过userId获取user信息
     *
     * @param userId
     * @return
     */
    User getUserByUserId(@Param("userId") Long userId);


    /**
     * 通过userId获取当前用户的姓名
     *
     * @param userId
     * @return
     */
    String getUserInfoNameByUserId(@Param("userId") Long userId);


    /**
     * 获取所有用户的uid
     *
     * @return
     */
    List<Long> getAllUserIds();


    /**
     * 查询project_user_relation 和user_role_relation的联表
     * 只查出当前projectId中的普通员工的uid集合（除去部门领导和项目组长）
     *
     * @param projectId
     * @return
     */
    List<Long> getUserIdsFromProjectOnlyStaff(@Param("projectId") String projectId);


    /**
     * 手机号快速注册时，给新用户默认设置权限为员工
     * 即 rid==3
     *
     * @param userId
     * @return
     */
    Boolean setDefaultUserRole(@Param("userId") Long userId);


    /**
     * 获取满足当前role的所有用户的userIds
     *
     * @param roleNUm
     * @return
     */
    List<Long> getUserIdsSatisfyRole(@Param("roleNum") Integer roleNUm);


    /**
     * 去user_info表删除用户数据
     *
     * @param userId
     * @return
     */
    Boolean delUserInfoByUserId(@Param("userId") Long userId);

    /**
     * 去tb_user表删除用户数据
     *
     * @param userId
     * @return
     */
    Boolean delUserByUserId(@Param("userId") Long userId);

    /**
     * 去user_role_relation表删除用户数据
     *
     * @param userId
     * @return
     */
    Boolean delUserRoleInfoByUserId(@Param("userId") Long userId);

    /**
     * 去department_user_relation表删除用户数据
     *
     * @param userId
     * @return
     */
    Boolean delUserDepInfoByUserId(@Param("userId") Long userId);


    /**
     * 去project_user_relation表删除用户数据
     * @param userId
     * @return
     */
    Boolean delUserFormProjectByUserId(@Param("userId") Long userId);

    /**
     * 去role_user_relation表中查询出
     * 是项目经理（rid==2）而且不为 uid！= currentProjectLeaderId 的所有uid
     * @param currentProjectLeaderId
     * @return
     */
    List<Long> getAllProjectLeaderIds(@Param("currentProjectLeaderId") Long currentProjectLeaderId);


    /**
     * 通过userId获取用户的RoleNum
     * @param userId
     * @return
     */
    Integer getUserRoleNumByUserId(@Param("userId")Long userId);


    /**
     * 更改当前用户的权限
     * @param userRole
     * @param userId
     * @return
     */
    Boolean updateUserRole(@Param("userRole") int userRole, @Param("userId") Long userId);




}
