package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.NoteLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface NoteLogMapper extends BaseMapper<NoteLog> {
    /**
     * 通过noteId获取最新的noteLog
     *
     * @param NoteId
     * @return
     */
    NoteLog getLatestNoteLogByNoteId(@Param("noteId") String NoteId);
}
