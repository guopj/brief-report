package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.Project;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProjectMapper extends BaseMapper<Project> {
    /**
     * 根据项目名称获取当前项目除了自己以外的所有组员的uid
     * @param projectId
     * @return
     */
    List<Long> getAllUserInProject(@Param("projectId") String projectId,@Param("currentUserId") Long currentUserId );

    /**
     * 获取当前项目的所有组员id
     * @param projectId
     * @return
     */
    List<Long> getAllUserInProjectInc(@Param("projectId")String projectId);

    /**
     * 获取当前用户管理的所有项目
     * @param userId
     * @return
     */
    List<Project> getUserManagerProjectList(@Param("userId") Long userId);

    /**
     * 向project_user_relation中间表加入数据
     * @param userId
     * @param projectId
     * @return
     */
    Boolean addProjectMember(@Param("userId") Long userId,@Param("projectId") String projectId);

    /**
     * 向project_user_relation中间表删除数据
     * @param userId
     * @param projectId
     * @return
     */
    Boolean removeFormProject(@Param("userId") Long userId,@Param("projectId") String projectId);


    /**
     * 根据projectId去project)_user_relation表中删除
     * 对应的用户和项目的关联信息
     * @param projectId
     * @return
     */
    Boolean delProjectMemberByProjectId(@Param("projectId")String projectId);


}
