package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.RemindMessage;
import com.kinroy.briefreport.entity.TodoEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TodoEventMapper extends BaseMapper<TodoEvent> {
    /**
     * 批量完成待办事务
     * @param todoEventList
     * @return
     */
    Boolean doneTodoEvent(@Param("eventList") List<TodoEvent> todoEventList);

    Boolean delTodoEvent(@Param("eventList")List<TodoEvent>todoEventList);

}
