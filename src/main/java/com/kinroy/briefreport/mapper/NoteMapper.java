package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.Note;
import com.kinroy.briefreport.entity.NoteLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author kinroy
 */
@Mapper
public interface NoteMapper extends BaseMapper<Note> {

    /**
     * 批量发布日志
     * @param noteIds
     * @return
     */
    Integer batchScheduledPublishingNotes(Set<String> noteIds);

    /**
     * 发布或保存、定时发布单个日志数据
     * @param note
     * @return
     */
    Boolean publishOrSaveNote( Note note);


    /**
     * 获取对应noteId的日志的所有图片url
     * @param noteId
     * @return
     */
    List<String> getNoteImagesByNoteId(@Param("noteId")String noteId);


    /**
     * 获取对应publishUser的比传入日期还往后的发表日期的日志
     * @param publishTime
     * @param publishUserId
     * @return
     */
    Note getNoteAfterThisTime(@Param("publishTime")LocalDateTime publishTime,@Param("publishUserId")Long publishUserId,@Param("noteType")int noteType);

    /**
     * 获取在start-end这段时间的已经发布了的note日志
     *
     * @param publishStartTime
     * @param publishEndTime
     * @param publishUserId
     * @param noteType
     * @return
     */
    Note getNodeBetweenTheTime(@Param("publishStartTime")LocalDateTime publishStartTime,@Param("publishEndTime")LocalDateTime publishEndTime,@Param("publishUserId")Long publishUserId,@Param("noteType")int noteType);

    /**
     * 获取在start-end这段时间的已经发布了的note日志数，根据userId和noteType
     * @param publishStartTime
     * @param publishEndTime
     * @param publishUserId
     * @param noteType
     * @return
     */
    Integer countNodeBetweenTheTime(@Param("publishStartTime")LocalDateTime publishStartTime,@Param("publishEndTime")LocalDateTime publishEndTime,@Param("publishUserId")Long publishUserId,@Param("noteType")int noteType);

}
