package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.HandOverNode;
import com.kinroy.briefreport.entity.Task;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HandOverNodeMapper extends BaseMapper<HandOverNode> {
    /**
     * 获取当前任务的最新的任务节点（根据create_time字段来查出）
     * @param task 要查询最新节点的任务实体
     * @return
     */
    HandOverNode getLatestNode(Task task);

    /**
     * 保存节点数据
     * @param node
     * @return
     */
    Boolean saveNode(HandOverNode node);

    /**
     * 根据id修改节点数据
     * @param node
     * @return
     */
    Boolean updateByNodeId(HandOverNode node);

    /**
     * 根据taskId获取该任务的所有任务节点(升序排列)
     * @param taskId
     * @return
     */
    List<HandOverNode>  getHandOverNodePath(@Param("taskId") String taskId);

    /**
     * 删除所有为当前taskId的任务流转节点
     * @param taskId
     * @return
     */
    Boolean removeAllNodeByTaskId(@Param("taskId") String taskId);
}
