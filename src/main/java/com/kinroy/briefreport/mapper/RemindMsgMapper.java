package com.kinroy.briefreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.briefreport.entity.RemindMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RemindMsgMapper extends BaseMapper<RemindMessage> {
    /**
     * 根据userId获取到对应用户的未读信息
     * 未读表示 msg_status == 0
      * @param userId
     * @return
     */
    List<RemindMessage> getMyUnReadRemindMsg(@Param("userId") Long userId);

    /**
     * 已读单条提醒
     * @param tableId
     * @return
     */
    boolean readRemind(@Param("tableId") Integer tableId);

    /**
     * 根据table_id 批量修改 msg_status =1
     * @param remindMessageList
     * @return
     */
    Boolean readAllRemind(@Param("msgList")List<RemindMessage> remindMessageList);


}
