package com.kinroy.briefreport.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定统计方法运行时间的注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CalculateRuntime {
    //我们可以在这里定义一下注解的传参 和 对应参数的默认值
     String value() default "";
}

