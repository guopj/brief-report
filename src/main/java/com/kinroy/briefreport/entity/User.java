package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
/**
 * 用户/员工/部门领导/项目组长实体类
 * @author kinroy
 */
@TableName("tb_user")
public class User {
    private Long uid;
    private String name;
    private String salt;
    private String password;
    private  String phone;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    public User() {
    }

    public User(Long uid, String name, String salt, String password, String phone, LocalDateTime createTime) {
        this.uid = uid;
        this.name = name;
        this.salt = salt;
        this.password = password;
        this.phone = phone;
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * 设置
     * @param uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 设置
     * @param salt
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "User{uid = " + uid + ", name = " + name + ", salt = " + salt + ", password = " + password + ", phone = " + phone + ", createTime = " + createTime + "}";
    }
}
