package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;


/**
 * 部门实体类
 * @author kinroy
 */
@TableName("tb_department")
public class Department {

    @TableId(value = "department_id")
    private Long departmentId;
    @TableField(value = "department_name")
    private String departmentName;
    @TableField(value ="department_info" )
    private String departmenInfo;
    @TableField(value = "department_manager")
    private List<User> departmentManager;


    public Department() {
    }

    public Department(Long departmentId, String departmentName, String departmenInfo, List<User> departmentManager) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmenInfo = departmenInfo;
        this.departmentManager = departmentManager;
    }

    /**
     * 获取
     * @return departmentId
     */
    public Long getDepartmentId() {
        return departmentId;
    }

    /**
     * 设置
     * @param departmentId
     */
    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * 获取
     * @return departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * 设置
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * 获取
     * @return departmenInfo
     */
    public String getDepartmenInfo() {
        return departmenInfo;
    }

    /**
     * 设置
     * @param departmenInfo
     */
    public void setDepartmenInfo(String departmenInfo) {
        this.departmenInfo = departmenInfo;
    }

    /**
     * 获取
     * @return departmentManager
     */
    public List<User> getDepartmentManager() {
        return departmentManager;
    }

    /**
     * 设置
     * @param departmentManager
     */
    public void setDepartmentManager(List<User> departmentManager) {
        this.departmentManager = departmentManager;
    }

    @Override
    public String toString() {
        return "Department{departmentId = " + departmentId + ", departmentName = " + departmentName + ", departmenInfo = " + departmenInfo + ", departmentManager = " + departmentManager + "}";
    }
}
