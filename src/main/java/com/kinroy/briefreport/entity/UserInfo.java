package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 用户的详细个人信息
 *
 * @author 10161
 */
@TableName("user_info")
public class UserInfo {
    @TableId(value = "table_id", type = IdType.AUTO)
    private Long userInfoId;
    @TableField(value = "user_id")
    private Long userId;
    private String email;
    private String gender;
    @TableField(value = "avatar_image")
    private String avatarImage;
    private String introduction;
    @TableField(value = "info_name")
    private String infoName;
    @TableField(value = "job_type")
    private String jobType;


    public UserInfo() {
    }

    public UserInfo(Long userInfoId, Long userId, String email, String gender, String avatar_image, String introduction, String infoName, String JobType) {
        this.userInfoId = userInfoId;
        this.userId = userId;
        this.email = email;
        this.gender = gender;
        this.avatarImage = avatar_image;
        this.introduction = introduction;
        this.infoName = infoName;
        this.jobType = JobType;
    }

    /**
     * 获取
     * @return userInfoId
     */
    public Long getUserInfoId() {
        return userInfoId;
    }

    /**
     * 设置
     * @param userInfoId
     */
    public void setUserInfoId(Long userInfoId) {
        this.userInfoId = userInfoId;
    }

    /**
     * 获取
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取
     * @return gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 获取
     * @return avatar_image
     */
    public String getAvatarImage() {
        return avatarImage;
    }

    /**
     * 设置
     * @param avatarImage
     */
    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    /**
     * 获取
     * @return introduction
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置
     * @param introduction
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取
     * @return infoName
     */
    public String getInfoName() {
        return infoName;
    }

    /**
     * 设置
     * @param infoName
     */
    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    /**
     * 获取
     * @return JobType
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * 设置
     * @param JobType
     */
    public void setJobType(String JobType) {
        this.jobType = JobType;
    }

    public String toString() {
        return "UserInfo{userInfoId = " + userInfoId + ", userId = " + userId + ", email = " + email + ", gender = " + gender + ", avatar_image = " + avatarImage + ", introduction = " + introduction + ", infoName = " + infoName + ", JobType = " + jobType + "}";
    }
}
