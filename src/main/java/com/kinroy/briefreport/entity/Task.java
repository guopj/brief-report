package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

/**
 * 任务实体类
 *
 * @author kinroy
 */
@TableName("tb_task")
public class Task {
    @TableId(value = "task_id",type = IdType.ASSIGN_UUID)
    private String taskId; //任务id

    @TableField(value = "task_type")
    private int taskType; //任务类型（0：bug/1:需求/2:其他/3:优化/4:测试）

    @TableField(value = "task_Name")
    private String taskName; //任务名称

    @TableField(value = "create_userId")
    private Long createUserId; //任务创建者id

    @TableField(value = "current_taskHandlerId")
    private Long currentTaskHandlerId; //当前任务经办人id

    @TableField(value = "taskHandover_path")
    private String taskHandOverPath; //任务流转路径

    @TableField(value = "task_status")
    private int taskStatus; //任务状态 （0：创建未处理/1:处理中/2:挂起/3:已完成待测试/4:测试通过/5:测试未通过/6:任务关闭）

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime; //任务创建时间

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;//任务改动时间

    @TableField(value = "deadline_time ")
    private LocalDateTime deadlineTime;//任务截至时间

    @TableField(value = "completion_time")
    private LocalDateTime completionTime;//任务完成时间

    @TableField(value = "project_id")
    private String projectId; //任务所属的项目id

    @TableField(value = "task_priority ")
    private String taskPriority; //任务优先级（0：低/1:中/2:高/3:紧急）

    @TableField(value = "restart_times ")
    private int restartTimes; //任务重开次数（被打回自动+1）

    @TableField(value = "task_description")
    private String taskDescription; //任务描述

    @TableField(value = "task_solution")
    private String taskSolution; //解决方案


    public Task() {
    }

    public Task(String taskId, int taskType, String taskName, Long createUserId, Long currentTaskHandlerId, String taskHandOverPath, int taskStatus, LocalDateTime createTime, LocalDateTime updateTime, LocalDateTime deadlineTime, LocalDateTime CompletionTime, String ProjectId, String taskPriority, int restartTimes, String taskDescription, String taskSolution) {
        this.taskId = taskId;
        this.taskType = taskType;
        this.taskName = taskName;
        this.createUserId = createUserId;
        this.currentTaskHandlerId = currentTaskHandlerId;
        this.taskHandOverPath = taskHandOverPath;
        this.taskStatus = taskStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.deadlineTime = deadlineTime;
        this.completionTime = CompletionTime;
        this.projectId = ProjectId;
        this.taskPriority = taskPriority;
        this.restartTimes = restartTimes;
        this.taskDescription = taskDescription;
        this.taskSolution = taskSolution;
    }

    /**
     * 获取
     * @return taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取
     * @return taskType
     */
    public int getTaskType() {
        return taskType;
    }

    /**
     * 设置
     * @param taskType
     */
    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    /**
     * 获取
     * @return taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * 设置
     * @param taskName
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * 获取
     * @return createUserId
     */
    public Long getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置
     * @param createUserId
     */
    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取
     * @return currentTaskHandlerId
     */
    public Long getCurrentTaskHandlerId() {
        return currentTaskHandlerId;
    }

    /**
     * 设置
     * @param currentTaskHandlerId
     */
    public void setCurrentTaskHandlerId(Long currentTaskHandlerId) {
        this.currentTaskHandlerId = currentTaskHandlerId;
    }

    /**
     * 获取
     * @return taskHandOverPath
     */
    public String getTaskHandOverPath() {
        return taskHandOverPath;
    }

    /**
     * 设置
     * @param taskHandOverPath
     */
    public void setTaskHandOverPath(String taskHandOverPath) {
        this.taskHandOverPath = taskHandOverPath;
    }

    /**
     * 获取
     * @return taskStatus
     */
    public int getTaskStatus() {
        return taskStatus;
    }

    /**
     * 设置
     * @param taskStatus
     */
    public void setTaskStatus(int taskStatus) {
        this.taskStatus = taskStatus;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return updateTime
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置
     * @param updateTime
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取
     * @return deadlineTime
     */
    public LocalDateTime getDeadlineTime() {
        return deadlineTime;
    }

    /**
     * 设置
     * @param deadlineTime
     */
    public void setDeadlineTime(LocalDateTime deadlineTime) {
        this.deadlineTime = deadlineTime;
    }

    /**
     * 获取
     * @return CompletionTime
     */
    public LocalDateTime getCompletionTime() {
        return completionTime;
    }

    /**
     * 设置
     * @param CompletionTime
     */
    public void setCompletionTime(LocalDateTime CompletionTime) {
        this.completionTime = CompletionTime;
    }

    /**
     * 获取
     * @return ProjectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 设置
     * @param ProjectId
     */
    public void setProjectId(String ProjectId) {
        this.projectId = ProjectId;
    }

    /**
     * 获取
     * @return taskPriority
     */
    public String getTaskPriority() {
        return taskPriority;
    }

    /**
     * 设置
     * @param taskPriority
     */
    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    /**
     * 获取
     * @return restartTimes
     */
    public int getRestartTimes() {
        return restartTimes;
    }

    /**
     * 设置
     * @param restartTimes
     */
    public void setRestartTimes(int restartTimes) {
        this.restartTimes = restartTimes;
    }

    /**
     * 获取
     * @return taskDescription
     */
    public String getTaskDescription() {
        return taskDescription;
    }

    /**
     * 设置
     * @param taskDescription
     */
    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    /**
     * 获取
     * @return taskSolution
     */
    public String getTaskSolution() {
        return taskSolution;
    }

    /**
     * 设置
     * @param taskSolution
     */
    public void setTaskSolution(String taskSolution) {
        this.taskSolution = taskSolution;
    }

    @Override
    public String toString() {
        return "Task{taskId = " + taskId + ", taskType = " + taskType + ", taskName = " + taskName + ", createUserId = " + createUserId + ", currentTaskHandlerId = " + currentTaskHandlerId + ", taskHandOverPath = " + taskHandOverPath + ", taskStatus = " + taskStatus + ", createTime = " + createTime + ", updateTime = " + updateTime + ", deadlineTime = " + deadlineTime + ", CompletionTime = " + completionTime + ", ProjectId = " + projectId + ", taskPriority = " + taskPriority + ", restartTimes = " + restartTimes + ", taskDescription = " + taskDescription + ", taskSolution = " + taskSolution + "}";
    }
}
