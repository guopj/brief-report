package com.kinroy.briefreport.entity;

/**
 * 穿梭框数据展示实体
 * @author kinroy
 */
public class TransferItem {
    private Long key; //主要值
    private String label; //展示值
    private Boolean disabled; //是否禁用

    public TransferItem() {
    }

    public TransferItem(Long key, String label, Boolean disabled) {
        this.key = key;
        this.label = label;
        this.disabled = disabled;
    }

    /**
     * 获取
     * @return key
     */
    public Long getKey() {
        return key;
    }

    /**
     * 设置
     * @param key
     */
    public void setKey(Long key) {
        this.key = key;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 获取
     * @return disabled
     */
    public Boolean getDisabled() {
        return disabled;
    }

    /**
     * 设置
     * @param disabled
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String toString() {
        return "TransferItem{key = " + key + ", label = " + label + ", disabled = " + disabled + "}";
    }
}
