package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

/**
 * @Author kinroy
 * @create 2024-05-30  15:05
 * @Description 待办事项实体
 */
@TableName("tb_todoEvent")
public class TodoEvent {
    @TableId(value = "event_id",type = IdType.ASSIGN_UUID)
    private String eventId;// 待办事项主键

    @TableField(value = "event_context")
    private String context;// 待办事项文本描述

    @TableField(value = "event_status")
    private Integer eventStatus;//待办事项状态 0:未完成 1:完成

    @TableField(value = "user_id")
    private Long userId;// 所属用户id

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime; //创建时间
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime; //更新时间
    @TableField(value = "deadline_time ")
    private LocalDateTime deadlineTime; //设置完成时间


    public TodoEvent() {
    }

    public TodoEvent(String eventId, String context, Integer eventStatus, Long userId, LocalDateTime createTime, LocalDateTime updateTime, LocalDateTime deadlineTime) {
        this.eventId = eventId;
        this.context = context;
        this.eventStatus = eventStatus;
        this.userId = userId;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.deadlineTime = deadlineTime;
    }

    /**
     * 获取
     * @return eventId
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * 设置
     * @param eventId
     */
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    /**
     * 获取
     * @return context
     */
    public String getContext() {
        return context;
    }

    /**
     * 设置
     * @param context
     */
    public void setContext(String context) {
        this.context = context;
    }

    /**
     * 获取
     * @return eventStatus
     */
    public Integer getEventStatus() {
        return eventStatus;
    }

    /**
     * 设置
     * @param eventStatus
     */
    public void setEventStatus(Integer eventStatus) {
        this.eventStatus = eventStatus;
    }

    /**
     * 获取
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return updateTime
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置
     * @param updateTime
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取
     * @return deadlineTime
     */
    public LocalDateTime getDeadlineTime() {
        return deadlineTime;
    }

    /**
     * 设置
     * @param deadlineTime
     */
    public void setDeadlineTime(LocalDateTime deadlineTime) {
        this.deadlineTime = deadlineTime;
    }

    public String toString() {
        return "TodoEvent{eventId = " + eventId + ", context = " + context + ", eventStatus = " + eventStatus + ", userId = " + userId + ", createTime = " + createTime + ", updateTime = " + updateTime + ", deadlineTime = " + deadlineTime + "}";
    }
}
