package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

/**
 * 项目实体类
 * @author kinroy
 */
@TableName("tb_project")
public class Project {
    @TableId(value = "project_id", type = IdType.ASSIGN_UUID)
    private String projectId;//项目id

    @TableField(value = "project_name")
    private String projectName;//项目名称

    @TableField(value = "create_userId")
    private Long createUserId;//项目创建人id

    @TableField(value = "project_leaderId")
    private Long projectLeaderId;//项目组长id

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime; //项目完成时间

    @TableField(value = "completion_time ",fill = FieldFill.INSERT)
    private LocalDateTime CompletionTime;//项目完成时间

    @TableField(value = "project_status")
    private int projectStatus;//项目状态（0：未开始/1:进行中/2:完成）


    public Project() {
    }

    public Project(String projectId, String projectName, Long createUserId, Long ProjectLeaderId, LocalDateTime createTime, LocalDateTime CompletionTime, int projectStatus) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.createUserId = createUserId;
        this.projectLeaderId = ProjectLeaderId;
        this.createTime = createTime;
        this.CompletionTime = CompletionTime;
        this.projectStatus = projectStatus;
    }

    /**
     * 获取
     * @return projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 设置
     * @param projectId
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * 获取
     * @return projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置
     * @param projectName
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * 获取
     * @return createUserId
     */
    public Long getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置
     * @param createUserId
     */
    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取
     * @return ProjectLeaderId
     */
    public Long getProjectLeaderId() {
        return projectLeaderId;
    }

    /**
     * 设置
     * @param ProjectLeaderId
     */
    public void setProjectLeaderId(Long ProjectLeaderId) {
        this.projectLeaderId = ProjectLeaderId;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return CompletionTime
     */
    public LocalDateTime getCompletionTime() {
        return CompletionTime;
    }

    /**
     * 设置
     * @param CompletionTime
     */
    public void setCompletionTime(LocalDateTime CompletionTime) {
        this.CompletionTime = CompletionTime;
    }

    /**
     * 获取
     * @return projectStatus
     */
    public int getProjectStatus() {
        return projectStatus;
    }

    /**
     * 设置
     * @param projectStatus
     */
    public void setProjectStatus(int projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String toString() {
        return "Project{projectId = " + projectId + ", projectName = " + projectName + ", createUserId = " + createUserId + ", ProjectLeaderId = " + projectLeaderId + ", createTime = " + createTime + ", CompletionTime = " + CompletionTime + ", projectStatus = " + projectStatus + "}";
    }
}
