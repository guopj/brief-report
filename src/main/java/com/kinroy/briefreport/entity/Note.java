package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 工作日志父类
 *
 * @author kinroy
 */
@TableName("tb_note")
public class Note {
    @TableId(value = "note_id")
    private String noteId; //日志id
    @TableField(value = "note_name")
    private String noteName; //日志名称
    @TableField(value = "note_type")
    private Integer noteType; //日志类型（0：日报、1：周报、2：月度总结）
    @TableField(value = "publish_userid")
    private Long publishUserId; //发布者Id
    @TableField(value = "note_content")
    private String noteContent; //日志文本内容
    @TableField(exist = false)
    private List<String> noteImages; //日志的图像urls（不保存在tb_note表中，单独用一张表存）
    @TableField(value = "note_status")
    private Integer noteStatus; //日志状态 （0：保存未发布、2：保存定时发布、1：保存已发布）
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime; //创建日志时间
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime; //更新日志时间
    @TableField(value = "publish_time ")
    private LocalDateTime publishTime; //发布日志时间


    public Note() {
    }


    public Note(String noteId, String noteName, Integer noteType, Long publishUserId, String noteContent, List<String> noteImages, Integer noteStatus, LocalDateTime createTime, LocalDateTime updateTime, LocalDateTime publishTime) {
        this.noteId = noteId;
        this.noteName = noteName;
        this.noteType = noteType;
        this.publishUserId = publishUserId;
        this.noteContent = noteContent;
        this.noteImages = noteImages;
        this.noteStatus = noteStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.publishTime = publishTime;
    }

    /**
     * 获取
     * @return noteId
     */
    public String getNoteId() {
        return noteId;
    }

    /**
     * 设置
     * @param noteId
     */
    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    /**
     * 获取
     * @return noteName
     */
    public String getNoteName() {
        return noteName;
    }

    /**
     * 设置
     * @param noteName
     */
    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    /**
     * 获取
     * @return noteType
     */
    public Integer getNoteType() {
        return noteType;
    }

    /**
     * 设置
     * @param noteType
     */
    public void setNoteType(Integer noteType) {
        this.noteType = noteType;
    }

    /**
     * 获取
     * @return publishUserId
     */
    public Long getPublishUserId() {
        return publishUserId;
    }

    /**
     * 设置
     * @param publishUserId
     */
    public void setPublishUserId(Long publishUserId) {
        this.publishUserId = publishUserId;
    }

    /**
     * 获取
     * @return noteContent
     */
    public String getNoteContent() {
        return noteContent;
    }

    /**
     * 设置
     * @param noteContent
     */
    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    /**
     * 获取
     * @return noteImages
     */
    public List<String> getNoteImages() {
        return noteImages;
    }

    /**
     * 设置
     * @param noteImages
     */
    public void setNoteImages(List<String> noteImages) {
        this.noteImages = noteImages;
    }

    /**
     * 获取
     * @return noteStatus
     */
    public Integer getNoteStatus() {
        return noteStatus;
    }

    /**
     * 设置
     * @param noteStatus
     */
    public void setNoteStatus(Integer noteStatus) {
        this.noteStatus = noteStatus;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return updateTime
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置
     * @param updateTime
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取
     * @return publishTime
     */
    public LocalDateTime getPublishTime() {
        return publishTime;
    }

    /**
     * 设置
     * @param publishTime
     */
    public void setPublishTime(LocalDateTime publishTime) {
        this.publishTime = publishTime;
    }

    public String toString() {
        return "Note{noteId = " + noteId + ", noteName = " + noteName + ", noteType = " + noteType + ", publishUserId = " + publishUserId + ", noteContent = " + noteContent + ", noteImages = " + noteImages + ", noteStatus = " + noteStatus + ", createTime = " + createTime + ", updateTime = " + updateTime + ", publishTime = " + publishTime + "}";
    }
}
