package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

/**
 * 消息提醒实体类
 *
 * @author kinroy
 */
@TableName("tb_remindMessage")
public class RemindMessage {
    @TableId(value = "table_id", type = IdType.AUTO)
    private Long tableId; //消息提醒主键
    @TableField(value = "msg_content")
    private String msgContent; //消息内容
    @TableField(value = "msg_status")
    private Integer msgStatus; //消息状态（0：未读，1：已读）
    @TableField(value = "user_id")
    private Long userId; //消息的用户归属
    @TableField(value = "create_time")
    private LocalDateTime createTime; //消息记录创建时间


    public RemindMessage() {
    }

    public RemindMessage(Long tableId, String msgContent, Integer msgStatus, Long userId, LocalDateTime createTime) {
        this.tableId = tableId;
        this.msgContent = msgContent;
        this.msgStatus = msgStatus;
        this.userId = userId;
        this.createTime = createTime;
    }

    /**
     * 获取
     * @return tableId
     */
    public Long getTableId() {
        return tableId;
    }

    /**
     * 设置
     * @param tableId
     */
    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    /**
     * 获取
     * @return msgContent
     */
    public String getMsgContent() {
        return msgContent;
    }

    /**
     * 设置
     * @param msgContent
     */
    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    /**
     * 获取
     * @return msgStatus
     */
    public Integer getMsgStatus() {
        return msgStatus;
    }

    /**
     * 设置
     * @param msgStatus
     */
    public void setMsgStatus(Integer msgStatus) {
        this.msgStatus = msgStatus;
    }

    /**
     * 获取
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String toString() {
        return "RemindMessage{tableId = " + tableId + ", msgContent = " + msgContent + ", msgStatus = " + msgStatus + ", userId = " + userId + ", createTime = " + createTime + "}";
    }
}
