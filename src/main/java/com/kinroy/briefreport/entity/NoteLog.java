package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

/**
 * 日志操作的记录实体类
 *
 * @author kinroy
 */
@TableName("note_log")
public class NoteLog {
    @TableId(value = "table_id", type = IdType.AUTO)
    private String tableId; //log的主键
    @TableField(value = "note_id")
    private String noteId; //此条日志关联的Note主键
    @TableField(value = "operation")
    private String operation; //操作记录（1：保存日志未发表，2：发布日志，3：定时发布日志，4：编辑日志）
    @TableField(value = "operation_version")
    private Integer operationVersion; //操作版本号
    @TableField(value = "operation_time")
    private LocalDateTime operationTime; //操作时间


    public NoteLog() {
    }

    public NoteLog(String tableId, String noteId, String operation, Integer operationVersion, LocalDateTime operationTime) {
        this.tableId = tableId;
        this.noteId = noteId;
        this.operation = operation;
        this.operationVersion = operationVersion;
        this.operationTime = operationTime;
    }

    /**
     * 获取
     * @return tableId
     */
    public String getTableId() {
        return tableId;
    }

    /**
     * 设置
     * @param tableId
     */
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    /**
     * 获取
     * @return noteId
     */
    public String getNoteId() {
        return noteId;
    }

    /**
     * 设置
     * @param noteId
     */
    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    /**
     * 获取
     * @return operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * 设置
     * @param operation
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * 获取
     * @return operationVersion
     */
    public Integer getOperationVersion() {
        return operationVersion;
    }

    /**
     * 设置
     * @param operationVersion
     */
    public void setOperationVersion(Integer operationVersion) {
        this.operationVersion = operationVersion;
    }

    /**
     * 获取
     * @return operationTime
     */
    public LocalDateTime getOperationTime() {
        return operationTime;
    }

    /**
     * 设置
     * @param operationTime
     */
    public void setOperationTime(LocalDateTime operationTime) {
        this.operationTime = operationTime;
    }

    public String toString() {
        return "NoteLog{tableId = " + tableId + ", noteId = " + noteId + ", operation = " + operation + ", operationVersion = " + operationVersion + ", operationTime = " + operationTime + "}";
    }
}
