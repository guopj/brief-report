package com.kinroy.briefreport.entity;

public class SerlectTest {
    private String value;
    private String label;

    public SerlectTest() {
    }

    public SerlectTest(String value, String label) {
        this.value = value;
        this.label = label;
    }

    /**
     * 获取
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 设置
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    public String toString() {
        return "SerlectTest{value = " + value + ", label = " + label + "}";
    }
}
