package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("tb_role")
public class Role {
    @TableId(value = "rid")
    private Long rid;
    @TableField(value = "rname")
    private String roleName;
    @TableField(value = "rdesc")
    private String roleDesc;

    public Role() {
    }

    public Role(Long rid, String roleName, String roleDesc) {
        this.rid = rid;
        this.roleName = roleName;
        this.roleDesc = roleDesc;
    }

    /**
     * 获取
     *
     * @return rid
     */
    public Long getRid() {
        return rid;
    }

    /**
     * 设置
     *
     * @param rid
     */
    public void setRid(Long rid) {
        this.rid = rid;
    }

    /**
     * 获取
     *
     * @return roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置
     *
     * @param roleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取
     *
     * @return roleDesc
     */
    public String getRoleDesc() {
        return roleDesc;
    }

    /**
     * 设置
     *
     * @param roleDesc
     */
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public String toString() {
        return "Role{rid = " + rid + ", roleName = " + roleName + ", roleDesc = " + roleDesc + "}";
    }
}
