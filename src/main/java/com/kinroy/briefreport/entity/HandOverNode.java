package com.kinroy.briefreport.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

/**
 * 任务流转节点
 *
 * @author kinroy
 */
@TableName("tb_handovernode")
public class HandOverNode {
    @TableId(value = "node_id", type = IdType.ASSIGN_UUID)
    private String nodeId;//节点id

    @TableField(value = "task_handlerid")
    private Long taskHandlerId; //经办人id

    @TableField(value = "task_status")
    private int taskStatus; //任务状态 （0：创建未处理/1:处理中/2:挂起/3:已完成待测试/4:测试通过/5:测试未通过/6:任务关闭）

    /*@TableField(value = "next_nodeid ")
    private String nextNodeId; //下一个node节点的id指针*/

    @TableField(value = "task_id")
    private String taskId; //任务id

    @TableField(value = "handover_num")
    private int handOverNum;//流转序号

    @TableField(value = "create_time ", fill = FieldFill.INSERT)
    private LocalDateTime createTime; //节点创建时间

    //kinroy 2023.12.18 为了节点显示经办人姓名加的字段
    @TableField(value = "task_handlerName")
    private String taskHandlerName;


    public HandOverNode() {
    }

    public HandOverNode(String nodeId, Long taskHandlerId, int taskStatus, String taskId, int handOverNum, LocalDateTime createTime, String taskHandlerName) {
        this.nodeId = nodeId;
        this.taskHandlerId = taskHandlerId;
        this.taskStatus = taskStatus;
        this.taskId = taskId;
        this.handOverNum = handOverNum;
        this.createTime = createTime;
        this.taskHandlerName = taskHandlerName;
    }

    /**
     * 获取
     *
     * @return nodeId
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * 设置
     *
     * @param nodeId
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * 获取
     *
     * @return taskHandlerId
     */
    public Long getTaskHandlerId() {
        return taskHandlerId;
    }

    /**
     * 设置
     *
     * @param taskHandlerId
     */
    public void setTaskHandlerId(Long taskHandlerId) {
        this.taskHandlerId = taskHandlerId;
    }

    /**
     * 获取
     *
     * @return taskStatus
     */
    public int getTaskStatus() {
        return taskStatus;
    }

    /**
     * 设置
     *
     * @param taskStatus
     */
    public void setTaskStatus(int taskStatus) {
        this.taskStatus = taskStatus;
    }

    /**
     * 获取
     *
     * @return taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * 设置
     *
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取
     *
     * @return handOverNum
     */
    public int getHandOverNum() {
        return handOverNum;
    }

    /**
     * 设置
     *
     * @param handOverNum
     */
    public void setHandOverNum(int handOverNum) {
        this.handOverNum = handOverNum;
    }

    /**
     * 获取
     *
     * @return createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    /**
     * 设置
     *
     * @param createTime
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取
     *
     * @return taskHandlerName
     */
    public String getTaskHandlerName() {
        return taskHandlerName;
    }

    /**
     * 设置
     *
     * @param taskHandlerName
     */
    public void setTaskHandlerName(String taskHandlerName) {
        this.taskHandlerName = taskHandlerName;
    }

    @Override
    public String toString() {
        return "HandOverNode{nodeId = " + nodeId + ", taskHandlerId = " + taskHandlerId + ", taskStatus = " + taskStatus + ", taskId = " + taskId + ", handOverNum = " + handOverNum + ", createTime = " + createTime + ", taskHandlerName = " + taskHandlerName + "}";
    }
}
