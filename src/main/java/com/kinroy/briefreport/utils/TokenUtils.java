package com.kinroy.briefreport.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.exception.ServiceException;
import com.kinroy.briefreport.mapper.UserMapper;
import com.kinroy.briefreport.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.condition.RequestConditionHolder;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * token工具类
 *
 * @author kinroy
 */
@Component
public class TokenUtils {

    @Resource
    private UserMapper userMapper;

    private static UserMapper staticUserMapper;


    @PostConstruct
    public void setStaticUserMapper() {
        //@PostConstruct 加上这个注解，表示该方法在这个类被spring实例的时候只会被执行一次
        staticUserMapper = userMapper;
    }

    /**
     * 通过传入用户id和 签名来生成相应的token
     *
     * @param uid  用户id
     * @param sign 签名（密码、电话都可以，用来校验用）
     * @return
     */
    public static String generateToken(Long uid, String sign) {
        String token = JWT.create().withAudience(String.valueOf(uid)) //只在token的载荷中放入 uid，所有在校验的时候取出的下标为0
                .withExpiresAt(DateUtil.offsetHour(new Date(), 2)) //设置token的过期时间为2小时
                .sign(Algorithm.HMAC256(sign)); //指定的加密算法为h256，用密码做sign，则解密的时候也要用相同的解密方式和sign
        return token;
    }


    /**
     * 获取当前登录的user信息
     *
     * @return
     */
    public static User getCurrentUser() {
        //1.获取request对象
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            //2.从request对象中获取出userid，并去db中查询
            String token = request.getHeader("token");
            if (StrUtil.isNotBlank(token)) {
                String userId = JWT.decode(token).getAudience().get(0);
                User user = staticUserMapper.selectById(userId);
                if (user == null) {
                    return null;
                }
                return user;
            }
        } catch (JWTDecodeException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
}
