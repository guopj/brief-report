package com.kinroy.briefreport.utils;

import cn.hutool.core.util.StrUtil;

import java.util.regex.Pattern;

/**
 * uuid判断工具类
 * @author kinroy
 */
public class UUIDJudgmentTool {
    // 定义去除中划线的UUID的正则表达式
    private static final String UUID_REGEX = "^[0-9a-fA-F]{32}$";

    // 创建正则表达式模式
    private static final Pattern UUID_PATTERN = Pattern.compile(UUID_REGEX);
    /**
     * 判断当前字符串是否符合uuid规范（去掉中划线的uuid）
     * @param target
     * @return
     */
    public static boolean uuidMatch(String target){
        // 去除中划线
        String uuidWithoutHyphens = target.replaceAll("-", "");
        return UUID_PATTERN.matcher(uuidWithoutHyphens).matches();
    }
}
