package com.kinroy.briefreport.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import java.io.IOException;

public interface JsonDeserializer <LocalDateTime>{
    LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException;
}
