package com.kinroy.briefreport.utils;

import com.kinroy.briefreport.entity.User;

/**
 * @author kinroy
 */
public class UserHolder {
    //使用threadLocal来存储我们的用户id

    private static final ThreadLocal<User> tl = new ThreadLocal<>();

    public static void saveUser(User user){
        tl.set(user);
    }

    public static User getUser(){
        return tl.get();
    }

    public static void removeUser(){
        tl.remove();
    }
}
