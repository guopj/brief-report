package com.kinroy.briefreport.utils;

import java.security.SecureRandom;

/**
 * 验证码工具类
 *
 * @author kinroy
 */
public class VerificationCodeUtils {

    public static final char[] LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    public static final char[] NUMBERS = "1234567890".toCharArray();


    public static String getCode(int digit, int type) {
        //1:表示生成纯字母，大小写随机   2：表示生成纯数字
        SecureRandom random = new SecureRandom();
        String code = "";
        // 生成指定位数的随机验证码
        if (type == 1) {
            StringBuilder codeBuilder = new StringBuilder();
            for (int i = 0; i < digit; i++) {
                int randomIndex = random.nextInt(LETTERS.length);
                char randomChar = LETTERS[randomIndex];
                codeBuilder.append(randomChar);
            }
            code = codeBuilder.toString();
        } else if (type == 2) {
            StringBuilder codeBuilder = new StringBuilder();
            for (int i = 0; i < digit; i++) {
                int randomIndex = random.nextInt(NUMBERS.length);
                char randomChar = NUMBERS[randomIndex];
                codeBuilder.append(randomChar);
            }
            code = codeBuilder.toString();
        }
        return code;
    }
}
