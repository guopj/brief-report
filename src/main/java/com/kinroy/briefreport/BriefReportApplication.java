package com.kinroy.briefreport;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author kinroy
 */
@EnableScheduling
@SpringBootApplication
@Slf4j
public class BriefReportApplication {
    public static void main(String[] args) {
        log.info(" ____                          ___  ____                                   __      \n" +
                "/\\  _`\\          __          /'___\\/\\  _`\\                                /\\ \\__   \n" +
                "\\ \\ \\L\\ \\  _ __ /\\_\\     __ /\\ \\__/\\ \\ \\L\\ \\     __   _____     ___   _ __\\ \\ ,_\\  \n" +
                " \\ \\  _ <'/\\`'__\\/\\ \\  /'__`\\ \\ ,__\\\\ \\ ,  /   /'__`\\/\\ '__`\\  / __`\\/\\`'__\\ \\ \\/  \n" +
                "  \\ \\ \\L\\ \\ \\ \\/ \\ \\ \\/\\  __/\\ \\ \\_/ \\ \\ \\\\ \\ /\\  __/\\ \\ \\L\\ \\/\\ \\L\\ \\ \\ \\/ \\ \\ \\_ \n" +
                "   \\ \\____/\\ \\_\\  \\ \\_\\ \\____\\\\ \\_\\   \\ \\_\\ \\_\\ \\____\\\\ \\ ,__/\\ \\____/\\ \\_\\  \\ \\__\\\n" +
                "    \\/___/  \\/_/   \\/_/\\/____/ \\/_/    \\/_/\\/ /\\/____/ \\ \\ \\/  \\/___/  \\/_/   \\/__/\n" +
                "                                                        \\ \\_\\                      \n" +
                "                                                         \\/_/        (v1.0-SNAPSHOT)           ");
        log.info("简捷报系统已启动！你的企业级工作日志管理系统。");
        SpringApplication.run(BriefReportApplication.class,args);
    }
}
