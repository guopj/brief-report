package com.kinroy.briefreport.interceptor;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.util.StringUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kinroy.briefreport.entity.User;
import com.kinroy.briefreport.exception.ServiceException;
import com.kinroy.briefreport.mapper.UserMapper;
import com.kinroy.briefreport.service.IUserService;
import com.kinroy.briefreport.utils.UserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





public class JwtInterceptor implements HandlerInterceptor {

    @Autowired
    private IUserService userService;



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.先从request请求中获取到请求头中的token
        String token = request.getHeader("token");
        if (StrUtil.isBlank(token)) {
            //请求头没有token，则去请求参数中获取
            token = request.getParameter("token");
        }
        //2.校验token是否为空
        if (StrUtil.isBlank(token)) {
            throw new ServiceException("401", "请先完成登录");
        }

        //3.解析鉴权（1.从token中获取用户id，查db，校验用户是否存在； 2.如果存在，则db中会返回用户数据，我们通过
        // 返回的user信息的password 作为jwt的加签验证H256，校验一下传来的token是否正确，如果token中的password和
        //数据库中的加签验证通过，则放行。
        // ）

        //Audience类似于jwt的一个存储空间，在里面可以存一些信息
        String userId = "";
        try {
            userId = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException e) {
            //如果获取不到，抛异常了，我们就抛出我们的自定义异常
            throw new ServiceException("401", "请先完成登录");
        }
        //获取到了userid，通过userid去查db，看一下有没有这个user数据
        //todo:使用userid去查数据库查不到数据
        Integer id = Integer.valueOf(userId);
        LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(User::getUid,id);
        User user = userService.getOne(wrapper);
        //kinroy 2024.1.26 新增业务，查询到相应的user信息后存入theadLocal中，方便以后取用
        UserHolder.saveUser(user);

        //User user = userService.getById(id);

        if (user == null) {
            throw new ServiceException("401", "请先完成登录");
        }

        //通过查出来的user数据生成jwt校验器来校验传来的token是否正确
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(user.getPassword())).build();


        try {
            verifier.verify(token);
        } catch (JWTVerificationException e) {
            //如果校验不通过，就报异常
            throw new ServiceException("401", "请先完成登录");
        }
        //校验通过
        return true;
    }

}
