package com.kinroy.briefreport.enums.file;

/**
 * 各种文件文件名规范的枚举类
 * @author kinroy
 */

public enum FileNamingPrefix {
    //文件命名规则 ： 文件类型_上传用户主键_随机字符
    IMG_FILE("img_"), //图片
    ZIP_FILE("zip_"), //压缩文件
    HTML_FILE("html_"); //html页面

    private String pre;

    private FileNamingPrefix(String pre) {
        this.pre = pre;
    }


    FileNamingPrefix() {
    }




    /**
     * 获取
     * @return pre
     */
    public String getPre() {
        return pre;
    }

    /**
     * 设置
     * @param pre
     */
    public void setPre(String pre) {
        this.pre = pre;
    }


    @Override
    public String toString() {
        return "FileNamingPrefix{IMG_FILE = " + IMG_FILE + ", ZIP_FILE = " + ZIP_FILE + ", HTML_FILE = " + HTML_FILE + ", pre = " + pre + "}";
    }
}
