package com.kinroy.briefreport.enums.file;

/**
 * minio 文件上传的文件夹前缀
 *
 * @author kinroy
 */

public enum MinioFilePrefix {
    USERINFO_iMG("userInfoImg"), //用户信息图片文件夹名
    TASK_HTML("taskHtml"), //任务相关的html文件夹名
    TASK_ZIP("taskZip"),//任务相关的zip文件夹名
    TASK_IMG("taskImg");//任务相关的图片文件夹名
    private String prefix;


    MinioFilePrefix(String prefix) {
        this.prefix = prefix;
    }

    MinioFilePrefix() {
    }



    /**
     * 获取
     *
     * @return prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * 设置
     *
     * @param prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    @Override
    public String toString() {
        return "MinioFilePrefix{USERINFO_iMG = " + USERINFO_iMG + ", TASK_HTML = " + TASK_HTML + ", TASK_ZIP = " + TASK_ZIP + ", TASK_IMG = " + TASK_IMG + ", prefix = " + prefix + "}";
    }
}
