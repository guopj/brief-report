package com.kinroy.briefreport.enums.role;

public enum UserRole {
    DEPARTMENT_LEADERS(1), //部门领导
    PROJECT_LEADER(2), //项目组长
    STAFF(3); //普通员工


    private int code;


    private UserRole(int code) {
        this.code = code;
    }

    /**
     * 获取
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "UserRole{code = " + code + "}";
    }
}
