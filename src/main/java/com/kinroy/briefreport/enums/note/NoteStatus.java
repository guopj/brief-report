package com.kinroy.briefreport.enums.note;

/**
 * 日志状态枚举
 *
 * @author kinroy
 */

public enum NoteStatus {
    SAVE_BUT_NOT_PUBLISH(0), //保存但未发布
    SAVE_PUBLISH(1),  //已发布
    SAVE_REGULARLY_PUBLISH(2); //定时发布
    private int code;


    NoteStatus(int code) {
        this.code = code;
    }

    /**
     * 获取
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     *
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "NoteStatus{code = " + code + "}";
    }
}
