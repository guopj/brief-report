package com.kinroy.briefreport.enums.note;

/**
 * 日志类型枚举
 *
 * @author kinroy
 */

public enum NoteType {
    Daily(0), //日报
    Weekly(1), //周报
    Monthly(2); //月度总结

    private int code;

    NoteType(int code) {
        this.code = code;
    }


    /**
     * 获取
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     *
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    public String toString() {
        return "NoteType{Daily = " + Daily + ", Weekly = " + Weekly + ", Monthly = " + Monthly + ", code = " + code + "}";
    }
}
