package com.kinroy.briefreport.enums.remind;

/**
 * @author kinroy
 */
public enum RemindMsgStatus {
    UNREAD(0), //未读
    READ(1); //已读
    private int code;


    RemindMsgStatus(int code) {
        this.code = code;
    }

    /**
     * 获取
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     *
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "RemindMsgStatus{code = " + code + "}";
    }
}
