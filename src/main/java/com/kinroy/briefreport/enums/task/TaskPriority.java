package com.kinroy.briefreport.enums.task;

public enum TaskPriority {
    LOW(0), //低
    MEDIUM (1), //中
    HIGH(2), //高
    URGENT(3); //紧急
    private int code;


   private TaskPriority(int code) {
        this.code = code;
    }

    /**
     * 获取
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "TaskPriority{code = " + code + "}";
    }
}
