package com.kinroy.briefreport.enums.task;

/**
 * 任务类型枚举类
 * @author kinroy
 */

public enum TaskType {
    BUG(0),  //bug
    DEV(1), //需求开发
    OTHER(2), //其他
    OPTIMIZATION(3), //优化
    TEST(4); //测试
    private int code;
  private TaskType(int code) {
        this.code = code;
    }


    /**
     * 获取
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "TaskType{BUG = " + BUG + ", DEV = " + DEV + ", OTHER = " + OTHER + ", OPTIMIZATION = " + OPTIMIZATION + ", TEST = " + TEST + ", code = " + code + "}";
    }
}
