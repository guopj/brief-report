package com.kinroy.briefreport.enums.task;

public enum TaskStatus {
    CREATE_UNPROCESSED(0), //创建未处理
    PROCESSING(1), //处理中
    PENDING(2), //挂起
    COMPLETED_WAIT_TESTING(3), //已完成待测试
    TEST_PASSED(4),//测试通过
    TEST_FAILD(5),//测试未通过
    CLOSED(6);//任务关闭


    private int code;


    private TaskStatus(int code) {
        this.code = code;
    }

    /**
     * 获取
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     *
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "TaskStatus{code = " + code + "}";
    }
}
