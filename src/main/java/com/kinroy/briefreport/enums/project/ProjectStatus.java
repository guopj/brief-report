package com.kinroy.briefreport.enums.project;

public enum ProjectStatus {
    UNPROGRESS(0), //未开始
    PROGRESS(1), //进行中
    COMPLETE(2); //完成
    private int code;


   private ProjectStatus(int code) {
        this.code = code;
    }

    /**
     * 获取
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ProjectStatus{code = " + code + "}";
    }
}
