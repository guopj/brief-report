package com.kinroy.brefreport;

import cn.hutool.core.util.StrUtil;
import com.kinroy.briefreport.entity.User;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * minio测试类
 */
public class MinioTest {
    public static void main(String[] args) throws IOException {
        //测试通过
        //使用输入流读取本地文件
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("E:\\list.html");
            //创建minio客户端与我们服务器上的minio进行连接
            MinioClient minioClient = MinioClient.builder()
                    .credentials("minioadmin", "minioadmin")
                    .endpoint("http://127.0.0.1:9000").build();

            //1.通过客户端进行文件上传,先将文件封装成上传的格式
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object("list.html") //文件名
                    .contentType("text/html") //文件类型
                    .bucket("briefreport") //桶名称
                    .stream(fileInputStream, fileInputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
