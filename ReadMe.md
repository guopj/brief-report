<p align="center">
    <img alt="CoreShoplogo" src="src/main/resources/mdImages/logo.png" width="500" height="200" style="margin-bottom:10px;">
</p>
<h1>BriefReport-简捷报</h1>

![star](https://gitee.com/CoreUnion/CoreShop/badge/star.svg) [![fork](https://gitee.com/CoreUnion/CoreShop/badge/fork.svg)](https://gitee.com/kinroy/brief-report)
[![](https://img.shields.io/badge/Author-kinroy-orange.svg)](https://gitee.com/kinroy)
[![](https://img.shields.io/badge/version-v1.0-brightgreen.svg)](https://gitee.com/kinroy/brief-report)

## 项目介绍

简捷报是一款基于Spring Boot开发的企业工单流转和工作日志管理系统，旨在优化企业内部的工作流程和任务管理。其功能类似于JIRA，但更加注重简洁易用性，适合中小型企业以及团队的日常工作需求。简捷报通过集成工单管理、任务分配、进度跟踪、和工作日志记录等功能，为企业提供一个高效的工作协同平台。它具备直观的用户界面，强大的权限管理，以及灵活的工作流配置，使得团队可以更加高效地进行项目管理和任务执行。

## 功能介绍

对于简捷报这一系统的设计，可以通过3大模块来实现的上述的系统功能，分别是工作中心、日志中心、管理中心，而对于这三大模块具体设计如下：
一、	工作中心：
①	任务汇总：系统用户能通过任务汇总看到自己所有工作任务的完成状态，通过图表和表格详细展示汇总。
②	我的任务：用户可再我的任务中看到自己代办/未完成的任务，可对任务进行多种流程操作，包括分配/完成/挂起/测试等，是工作中心的主要功能区。
③	任务管理：任务管理模块为项目组长及以上角色的用户能够访问和使用，主要功能是对自己管理的项目的整体工作完成情况的管理还有所管理的项目管理。
二、	日志中心：
①	日志管理：用户对自己需提交的日报/周报/月度总结的管理模块。
②	我的日志：用户通过我的日志进行日志的编辑上传和保存。
三、	管理中心：
①	员工管理：对员工数据进行增删改查和权限分配。
②	日志管理：对当前管理员用户部门的用户的日志进行增删改查管理以及日志的详情查看。
③	项目管理：对项目的增删改查管理。

## 系统界面（部分展示）

![image.png](src/main/resources/mdImages/workBarch.png)

![image.png](src/main/resources/mdImages/myTasks.png)

![image.png](src/main/resources/mdImages/myNote.png)

![image.png](src/main/resources/mdImages/userInfo.png)


## 快速运行

### 后端

后端项目要本地运行需要本地先启动mysql、redis、minio。

启动完成后，将项目中的application.yml文件中的连接配置修改您本地即可。

### 前端

前端项目需要下载好node.js和vue、vue-cil

在终端中执行以下指令即可

```bash
npm install
npm i @kangc/v-md-editor -S
npm run sever
```

## 数据库

运行 src/main/resources/briefreport.sql 文件即可快速构建数据库表结构。
